package com.innovecs.greenhousetozohopeoplesynchronize;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GreenhouseToZohopeopleSynchronizeApplication {

    public static void main(String[] args) {
        SpringApplication.run(GreenhouseToZohopeopleSynchronizeApplication.class, args);
    }

}
