package com.innovecs.greenhousetozohopeoplesynchronize.dto;

import lombok.Data;

import java.sql.Date;

@Data
public class EmployeeInsertDto {

    private String name;
    private String email;
    private String mobilePhone;
    private Date birthdayDate;
}
