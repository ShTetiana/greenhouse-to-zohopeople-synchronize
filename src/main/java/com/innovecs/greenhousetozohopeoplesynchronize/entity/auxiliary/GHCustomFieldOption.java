package com.innovecs.greenhousetozohopeoplesynchronize.entity.auxiliary;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
public class GHCustomFieldOption {

    @Id
    private Long id;
    private String name;
    private Integer priority;

}
