package com.innovecs.greenhousetozohopeoplesynchronize.entity.relations;

import com.fasterxml.jackson.annotation.*;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZPDepartment;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "parent_id"
})
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Entity
@Table(name = "zp_gh_department")
@NoArgsConstructor

public class GHDepartment {


    @Id
    private Long zohoID;

    private Long id;// green house id

    private String name;

    @JsonProperty("parent_id")
    private Long ghParentId;

    @JsonIgnore
    private String department;

    private Boolean paired;

    private Long zohoParentId;

    private Integer level;

    public GHDepartment(ZPDepartment zpDepartment, Long ghid, String ghName) {
        this(zpDepartment);
        this.id = ghid;
        this.name = ghName;
        this.paired = true;
    }

    public GHDepartment(ZPDepartment zpDepartment) {
        this.zohoID = zpDepartment.getZohoID();
        this.department = zpDepartment.getDepartment();
        this.zohoParentId = zpDepartment.getParentDepartmentID();
        this.level = zpDepartment.getLevel();
        this.paired = false;
    }

    public String toString() {
        return " id: " + this.id + "\t name :" + this.name + " parent Id: " + ghParentId;
    }
}
