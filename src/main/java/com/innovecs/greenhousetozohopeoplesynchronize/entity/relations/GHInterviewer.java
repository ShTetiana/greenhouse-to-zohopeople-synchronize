package com.innovecs.greenhousetozohopeoplesynchronize.entity.relations;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZPEmployee;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "first_name",
        "last_name",
        "primary_email_address",
        "disabled",
        "site_admin"})
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Data
@Table(name = "zp_gh_interviewer")
@NoArgsConstructor
public class GHInterviewer {


    private Long zohoID;

    private String innovecsStatus;

    @JsonProperty("id")
    public Long id;

    @JsonProperty("name")
    public String name;

    @JsonProperty("first_name")
    public String firstName;

    @JsonProperty("last_name")
    public String lastName;

    @Id
    @JsonProperty("primary_email_address")
    public String primaryEmailAddress;

    @JsonProperty("disabled")
    public Boolean disabled;

    @JsonProperty("site_admin")
    public Boolean siteAdmin;

    public GHInterviewer(ZPEmployee employee) {
        this.zohoID = employee.getZohoID();
        this.innovecsStatus = employee.getInnovecsStatus1();
        this.firstName = employee.getFirstName();
        this.lastName = employee.getLastName();
        this.primaryEmailAddress = employee.getEmailID();
    }

}
