package com.innovecs.greenhousetozohopeoplesynchronize.entity.relations;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "zp_gh_designation_title")

@Entity
@NoArgsConstructor

public class ZPDesignationGHTitleRelation {


    @Id
    protected Long zohoID;

    protected Long ghID;

    protected String name;

    private Integer priority;


    public ZPDesignationGHTitleRelation(Long zohoID, String name, Integer priority) {
        this.zohoID = zohoID;
        this.name = name;
        this.priority = priority;
    }
}
