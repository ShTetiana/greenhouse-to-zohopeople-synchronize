package com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Seniority",
        "SLA_for_Hiring_biz_days"
})
@Data
@Entity
@Table(name = "zp_seniority")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ZPSeniority {


    @JsonProperty("CreatedTime")
    public Timestamp createdTime;

    @JsonProperty("ModifiedTime")
    public Timestamp modifiedTime;

    @JsonProperty("ApprovalStatus")
    public String approvalStatus;

    @JsonProperty("Zoho_ID")
    @Id
    public Long zohoID;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "crawler_created_at", updatable = false)
    private Date created;


    @JsonProperty("Seniority")
    public String seniority;

    @JsonProperty("SLA_for_Hiring_biz_days")
    public Integer sLAForHiringBizDays;

    @JsonProperty("Slafor_hiring_cal_days_internal")
    public Integer SLAForHiringCalDaysInternal;

    @JsonProperty("Slafor_hiring_cal_days")
    public Integer SLAForHiringCalDays;


}
