package com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "zp_employee")
public class ZPEmployee {

    @Id
    private Long zohoID;

    private String lastName;

    private String firstName;

    public String mobile;

    public String employeestatus;

    private String otherEmail;

    private String emailID;

    private LocalDate dateOfBirth;

    private String innovecsStatus1;

    private Boolean ghInterviewer;

    public Long departmentID;

    private String department;

    private String normalizeNumber;
}
