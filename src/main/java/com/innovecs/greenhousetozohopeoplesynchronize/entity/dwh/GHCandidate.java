package com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "gh_candidate")
public class GHCandidate {

    @Id
    private Long crawlerId;
    private Long id;
    private String emailAddresses;
    private String tags;
}
