package com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "zp_location")
public class ZPLocation {

    @Id
    private Long zohoid;

    @JsonProperty("Salary_coefficient")
    private Double salaryCoefficient;

    @JsonProperty("City")
    private String city;

}