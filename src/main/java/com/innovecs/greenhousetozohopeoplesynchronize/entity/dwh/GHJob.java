package com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import java.time.LocalDateTime;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@Entity
@Table(name = "gh_job")
@JsonIgnoreProperties(ignoreUnknown = true)
public class GHJob {

    @Id
    public Long id;

    @JsonProperty("name")
    public String name;

    @JsonProperty("status")
    public String status;

    private String positionTitle;

    public Double locationCoefficient;

    public Double salaryMax;

    public LocalDateTime openedAt;

    public String officeName;
}