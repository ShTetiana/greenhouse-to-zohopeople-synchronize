package com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "CreatedTime",
        "Short_Title.ID",
        "Stream.ID",
        "Seniority.ID",
        "Technology.ID",
        "ApprovalStatus",
        "min3",
        "min2",
        "ModifiedTime",
        "Technology",
        "Zoho_ID",
        "Stream",
        "Grade.ID",
        "MailAlias",
        "Designation",
        "Job_Family",
        "Grade",
        "Tech_track",
        "SOW_Title",
        "min1",
        "Seniority",
        "ID1",
        "Job_Family.ID",
        "Short_Title",
        "max3",
        "max2",
        "Tech_track.ID",
        "max1",
        "SOW_Title.ID",
        "Current_reference"
})
@Data
@Entity
@Table(name = "zp_designation")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ZPDesignation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long crawlerId;

    @JsonProperty("CreatedTime")
    public Timestamp createdTime;

    @JsonProperty("ModifiedTime")
    public Timestamp modifiedTime;

    @JsonProperty("ApprovalStatus")
    public String approvalStatus;

    @NaturalId
    @JsonProperty("Zoho_ID")
    public Long zohoID;


    @JsonProperty("Short_Title.ID")
    public Long shortTitleID;

    @JsonProperty("Stream.ID")
    public Long streamID;

    @JsonProperty("Seniority.ID")
    public Long seniorityID;

    @JsonProperty("Technology.ID")
    public Long technologyID;

    @JsonProperty("min3")
    public Double min3;

    @JsonProperty("min2")
    public Double min2;

    @JsonProperty("Technology")
    public String technology;

    @JsonProperty("Stream")
    public String stream;

    @JsonProperty("Grade.ID")
    public Long gradeID;

    @JsonProperty("MailAlias")
    public String mailAlias;

    @JsonProperty("Designation")
    public String designation;

    @JsonProperty("Job_Family")
    public String jobFamily;

    @JsonProperty("Grade")
    public String grade;
    @JsonProperty("Tech_track")
    public String techTrack;
    @JsonProperty("SOW_Title")
    public String sOWTitle;
    @JsonProperty("min1")
    public Double min1;
    @JsonProperty("Seniority")
    public String seniority;
    @JsonProperty("ID1")
    public String iD1;
    @JsonProperty("Job_Family.ID")
    public Long jobFamilyID;
    @JsonProperty("Short_Title")
    public String shortTitle;
    @JsonProperty("max3")
    public Double max3;
    @JsonProperty("max2")
    public Double max2;
    @JsonProperty("Tech_track.ID")
    public Long techTrackID;
    @JsonProperty("max1")
    public Double max1;
    @JsonProperty("SOW_Title.ID")
    public Long sOWTitleID;

    @JsonProperty("Current_reference")
    public String currentReference;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "crawler_created_at", updatable = false)
    private Date created;
    @JsonProperty("min5")
    public Double min5;
    @JsonProperty("max5")
    public Double max5;
    @JsonProperty("min4")
    public Double min4;
    @JsonProperty("max4")
    public Double max4;
}