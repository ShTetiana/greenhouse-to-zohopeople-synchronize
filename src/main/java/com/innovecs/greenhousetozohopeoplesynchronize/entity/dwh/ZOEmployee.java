package com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;
import java.time.ZonedDateTime;


@JsonInclude
        (JsonInclude.Include.NON_NULL)

@JsonPropertyOrder({
        "Bot_ID",
        "Owner",
        "Email",
        "$currency_symbol",
        "Marital_status",
        "Gender",
        "Further_information",
        "Name",
        "Last_Activity_Time",
        "Record_Image",
        "Modified_By",
        "$review",
        "Airlines",
        "$state",
        "$process_flow",
        "Technology",
        "Full_name_UA",
        "id",
        "Status",
        "$approval",
        "Mobile_phone",
        "Modified_Time",
        "Created_Time",
        "Email_external",
        "Title",
        "$editable",
        "Email_private",
        "Inflow_Employee_Id",
        "Workplace",
        "Head_of_BU",
        "Hand",
        "Kids",
        "$orchestration",
        "Food_Drinks",
        "Skype",
        "Interests_Hobbies",
        "Car",
        "Birthday_date",
        "MS_Dynamics_project",
        "Adress",
        "Hotel"
})
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Entity
@Table(name = "zo_employee")
@NoArgsConstructor
public class ZOEmployee {


    @JsonProperty("Bot_ID")
    public String botID;

    @JsonProperty("Email")
    public String email;

    @JsonProperty("$currency_symbol")
    public String currencySymbol;

    @JsonProperty("Marital_status")
    public String maritalStatus;

    @JsonProperty("Gender")
    public String gender;

    @JsonProperty("Further_information")
    public String furtherInformation;

    @JsonProperty("Name")
    public String name;

    @JsonProperty("Last_Activity_Time")
    public ZonedDateTime lastActivityTime;

    @JsonProperty("Record_Image")
    public String recordImage;

    @JsonProperty("$review")
    public String review;

    @JsonProperty("Airlines")
    public String airlines;

    @JsonProperty("$state")
    public String state;

    @JsonProperty("$process_flow")
    public Boolean processFlow;

    @JsonProperty("Technology")
    public String technology;

    @JsonProperty("Full_name_UA")
    public String fullNameUA;

    @Id
    @JsonProperty("id")
    public Long id;

    @JsonProperty("Status")
    public String status;


    @JsonProperty("Mobile_phone")
    public String mobilePhone;

    @JsonProperty("Modified_Time")
    public ZonedDateTime modifiedTime;

    @JsonProperty("Created_Time")
    public ZonedDateTime createdTime;

    @JsonProperty("Email_external")
    public String emailExternal;

    @JsonProperty("Title")
    public String title;

    @JsonProperty("$editable")
    public Boolean editable;

    @JsonProperty("Email_private")
    public String emailPrivate;

    @JsonProperty("Inflow_Employee_Id")
    public Integer inflowEmployeeId;

    @JsonProperty("Workplace")
    public String workplace;

    @JsonProperty("Hand")
    private String hand;

    @JsonProperty("Kids")
    public String kids;

    @JsonProperty("$orchestration")
    public String orchestration;

    @JsonProperty("Food_Drinks")
    public String foodDrinks;

    @JsonProperty("Skype")
    public String skype;

    @JsonProperty("Interests_Hobbies")
    public String interestsHobbies;

    @JsonProperty("Car")
    public String car;

    @JsonProperty("Birthday_date")
    public Date birthdayDate;

    @JsonProperty("MS_Dynamics_project")
    public String mSDynamicsProject;

    @JsonProperty("Adress")
    public String adress;

    @JsonProperty("Hotel")
    public String hotel;

    public String headBuName;
    private Long headBuId;

    private String ownerName;
    private Long ownerId;

    private String createdByName;
    private Long createdById;

    private String modifiedByName;
    private Long modifiedById;

    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date created;

    private String normalizeNumber;

    public ZOEmployee(String name, String normalizeNumber, String email, String status, Date birthdayDate) {
        this.name = name;
        this.mobilePhone = normalizeNumber;
        this.email = email;
        this.status = (status.equals("Inactive"))? "Newcomer": status;
        this.birthdayDate = birthdayDate;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        ZOEmployee zo = (ZOEmployee) o;
        return (this.email.equals(zo.getEmail()));
    }
}
