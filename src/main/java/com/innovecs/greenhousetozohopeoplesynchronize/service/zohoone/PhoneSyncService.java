package com.innovecs.greenhousetozohopeoplesynchronize.service.zohoone;

import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZOEmployee;
import com.innovecs.greenhousetozohopeoplesynchronize.repository.dwh.ZOEmployeeRepository;
import com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse.EmailSender;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static com.innovecs.greenhousetozohopeoplesynchronize.util.SQLRequests.SQL_TERMINATED_ACTIVE_EMPLOYEES;


@Log4j2
@Service
@RequiredArgsConstructor
public class PhoneSyncService {

    private final ZOEmployeeRepository zoEmployeeRepository;

    private final ZOService zoService;

    private final JdbcTemplate jdbcTemplate;


    // add new employee with blablablablablabl json data

    //add new Employees

    //terminated employees should have only terminated status
    public void setTerminated() {
        String terminationObligations = "Termination obligations";
        List<ZOEmployee> zoEmployees = zoEmployeeRepository.getAllByStatus(terminationObligations);
        for (ZOEmployee zoEmployee : zoEmployees) {
            zoEmployee.setStatus("Terminated");
        }
        log.info("Number of employees with 'Termination obligations' instead 'Terminated':" + zoEmployees.size());
        if (zoEmployees.size() != 0) {
            zoService.updateRecords(zoEmployees);
        }
    }

    public void synchronizeTerminatedEmployees() {
        List<String> slackRecords = new ArrayList<>();
        List<String> zoEmployeeEmailsToTerminate = new ArrayList<>();
        List<String> zoEmployeeEmailsToActive = new ArrayList<>();
        jdbcTemplate.query(SQL_TERMINATED_ACTIVE_EMPLOYEES, (resultSet -> {
            if (resultSet.getString(3).equals("Terminated")) {
                zoEmployeeEmailsToTerminate.add(resultSet.getString(4));
            } else {
                zoEmployeeEmailsToActive.add(resultSet.getString(4));
            }
        }));

        List<ZOEmployee> zoEmployeesToTerminate = zoEmployeeRepository.getAllByEmailIn(new HashSet<>(zoEmployeeEmailsToTerminate));
        List<ZOEmployee> zoEmployeesToActivate = zoEmployeeRepository.getAllByEmailIn(new HashSet<>(zoEmployeeEmailsToActive));

        for (ZOEmployee zoEmployee : zoEmployeesToActivate) {
            zoEmployee.setStatus("Active");
            EmailSender.employeesWithUpdatedStatus.add(zoEmployee);
        }
        zoService.updateRecords(zoEmployeesToActivate);

        for (ZOEmployee zoEmployee : zoEmployeesToTerminate) {
            zoEmployee.setStatus("Terminated");
            EmailSender.employeesWithUpdatedStatus.add(zoEmployee);
        }
        zoService.updateRecords(zoEmployeesToTerminate);
    }

}
