package com.innovecs.greenhousetozohopeoplesynchronize.service.zohoone;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZOEmployee;
import com.innovecs.greenhousetozohopeoplesynchronize.repository.dwh.ZOEmployeeRepository;
import com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse.EmailSender;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.nio.file.Path;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


@Log4j2
@Service
@EnableScheduling
@RequiredArgsConstructor
public class ZOService {
    private final ZOEmployeeRepository zoEmployeeRepository;

    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;

    private static String accessToken = "Bearer ..."; // Paste the token here

    @Value("${innovecs.zo.refresh-token}")
    private String REFRESH_TOKEN;

    @Value("${innovecs.zo.client-id}")
    private String CLIENT_ID;

    @Value("${innovecs.zo.client-secret}")
    private String CLIENT_SECRET;

    private static final String EMPLOYEE_URL = "https://www.zohoapis.com/crm/v2/Employees";


    public void updateRecords(List<ZOEmployee> zoEmployees) {
        try {
            refreshToken();
            List<String> triggers = Collections.singletonList("approval");
            JSONArray jsonArray = new JSONArray();

            for (ZOEmployee zoEmployee : zoEmployees) {
                JSONObject record = new JSONObject();
                record.put("Name", zoEmployee.getName());
                record.put("Email", zoEmployee.getEmail());
                record.put("Mobile_phone", zoEmployee.getMobilePhone());
                record.put("id", zoEmployee.getId());
                record.put("Status", zoEmployee.getStatus());
                jsonArray.put(record);
            }
            JSONObject requestJSON = new JSONObject();
            requestJSON.put("data", jsonArray);
            requestJSON.put("trigger", triggers);

            OkHttpClient client = new OkHttpClient();

            log.info(requestJSON);

            RequestBody body = RequestBody.create(com.squareup.okhttp.MediaType.parse("application/javascript"), String.valueOf(requestJSON));
            System.out.println(body.toString());
            Request request = new Request.Builder()
                    .url(EMPLOYEE_URL)
                    .put(body)
                    .addHeader("Authorization", accessToken)
                    .addHeader("Content-Type", "application/javascript")
                    .addHeader("User-Agent", "PostmanRuntime/7.20.1")
                    .addHeader("Accept", "*/*")
                    .addHeader("Cache-Control", "no-cache")
                    .addHeader("Postman-Token", "89ecb889-6b7a-4a4a-ab21-96d289bdf59b,25aef2ae-b6ed-48c5-8c88-1355532e1b40")
                    .addHeader("Host", "www.zohoapis.com")
                    .addHeader("Accept-Encoding", "gzip, deflate")
                    .addHeader("Content-Length", "227")
                    .addHeader("Cookie", "1a99390653=097d1e8a5f4f275774525e2143b1676b; crmcsr=48a5bf09-ede5-4857-ab49-bd2c5e6ee503; JSESSIONID=40D510EC618E4E0127A333851DD9836E")
                    .addHeader("Connection", "keep-alive")
                    .addHeader("cache-control", "no-cache")
                    .build();

            Response response = client.newCall(request).execute();
            log.info("Update response: " + response.toString());
            response.body().close();

        } catch (
                Exception ex) {
            log.error(ex.getMessage());
        }

    }

    public void insertRecords(List<ZOEmployee> zoEmployees) {
        try {
            refreshToken();
            List<String> triggers = Arrays.asList("approval",  "workflow", "blueprint");
            JSONArray jsonArray = new JSONArray();

            for (ZOEmployee zoEmployee : zoEmployees) {
                JSONObject record = new JSONObject();
                record.put("Name", zoEmployee.getName());
                record.put("Email", zoEmployee.getEmail());
                record.put("Mobile_phone", zoEmployee.getMobilePhone());
                record.put("Status", zoEmployee.getStatus());
                jsonArray.put(record);
            }
            JSONObject requestJSON = new JSONObject();
            requestJSON.put("data", jsonArray);
            requestJSON.put("trigger", triggers);

            OkHttpClient client = new OkHttpClient();
            System.out.println(requestJSON);
            System.out.println();
            RequestBody body = RequestBody.create(com.squareup.okhttp.MediaType.parse("application/javascript"), String.valueOf(requestJSON));
            System.out.println(body.toString());
            Request request = new Request.Builder()
                    .url(EMPLOYEE_URL)
                    .post(body)
                    .addHeader("Authorization", accessToken)
                    .addHeader("Content-Type", "application/javascript")
                    .addHeader("User-Agent", "PostmanRuntime/7.20.1")
                    .addHeader("Accept", "*/*")
                    .addHeader("Cache-Control", "no-cache")
                    .addHeader("Postman-Token", "89ecb889-6b7a-4a4a-ab21-96d289bdf59b,25aef2ae-b6ed-48c5-8c88-1355532e1b40")
                    .addHeader("Host", "www.zohoapis.com")
                    .addHeader("Accept-Encoding", "gzip, deflate")
                    .addHeader("Content-Length", "227")
                    .addHeader("Cookie", "1a99390653=097d1e8a5f4f275774525e2143b1676b; crmcsr=48a5bf09-ede5-4857-ab49-bd2c5e6ee503; JSESSIONID=40D510EC618E4E0127A333851DD9836E")
                    .addHeader("Connection", "keep-alive")
                    .addHeader("cache-control", "no-cache")
                    .addHeader("Content", String.valueOf(requestJSON))
                    .build();


            Response response = client.newCall(request).execute();
            log.info("Insert response: " + response.toString());
            response.body().close();


        } catch (Exception ex) {
            ex.printStackTrace();
        }
        checkSuccessOfRequest(zoEmployees);
    }

    private void checkSuccessOfRequest(List<ZOEmployee> zoEmployees) {
        List<ZOEmployee> zoEmployeesInSystem = new ArrayList<>();
        try {
            String URL = "https://www.zohoapis.com/crm/v2/Employees/search";
            String CRITERIA = "(Email:in:emails)";
            StringBuilder stringBuilder = new StringBuilder();
            for (ZOEmployee zoEmployee : zoEmployees) {
                stringBuilder.append(zoEmployee.getEmail() + ",");
            }
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            CRITERIA = CRITERIA.replace("emails", stringBuilder.toString());

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(URL)
                    .queryParam("criteria", CRITERIA);

            ResponseEntity<Object> responseEntity = restTemplate.exchange(builder.toUriString(),
                    HttpMethod.GET, new HttpEntity<>(
                            new HttpHeaders() {{
                                set("Accept", MediaType.APPLICATION_JSON_VALUE);
                                set(AUTHORIZATION, accessToken);
                            }}), Object.class);
            log.info("The request has been sent on url: " + builder.toUriString());

            String str = objectMapper.writeValueAsString(responseEntity.getBody());
            JsonNode node = objectMapper.readValue(str, JsonNode.class).get("data");

            zoEmployeesInSystem = objectMapper.convertValue(
                    node,
                    new TypeReference<List<ZOEmployee>>() {
                    });

        } catch (Exception ex) {
            log.error(ex.getMessage());
        }


        if (!zoEmployeesInSystem.isEmpty()) {
            log.info("success");
            EmailSender.newEmployees.addAll(zoEmployeesInSystem);


        }

        zoEmployees.removeAll(zoEmployeesInSystem);

        if (!zoEmployees.isEmpty()) {
            List<String> strings = new ArrayList<>();
            strings.add("Employees who has not being added to ZohoOne:");
            for (ZOEmployee zoEmployee : zoEmployees) {
                strings.add(zoEmployee.getName());
            }
//            Path path = IntCheckService.writeIntoFile(strings, "V2EmployeesWithoutZohoOne");
//            IntCheckService.sendFile(path, "Employees who has not being added to ZohoOne:");
        }
    }

    private void refreshToken() {
        String url = "https://accounts.zoho.com/oauth/v2/token";
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                    .queryParam("refresh_token", REFRESH_TOKEN)
                    .queryParam("client_id", CLIENT_ID)
                    .queryParam("client_secret", CLIENT_SECRET)
                    .queryParam("grant_type", "refresh_token");


            ResponseEntity<Object> responseEntity = restTemplate.exchange(builder.toUriString(),
                    HttpMethod.POST, new HttpEntity<>(
                            new HttpHeaders() {{
                                set("Accept", MediaType.APPLICATION_JSON_VALUE);
                            }}), Object.class);

            String str = objectMapper.writeValueAsString(responseEntity.getBody());

            accessToken = "Bearer " + objectMapper.readValue(str, JsonNode.class).get("access_token").textValue();
            log.info("get new access token");
            log.info(new Timestamp(System.currentTimeMillis()));
            Thread.sleep(1000);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

}
