package com.innovecs.greenhousetozohopeoplesynchronize.service.zohoone;


import com.innovecs.greenhousetozohopeoplesynchronize.dto.EmployeeInsertDto;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZOEmployee;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZPEmployee;
import com.innovecs.greenhousetozohopeoplesynchronize.repository.dwh.ZOEmployeeRepository;
import com.innovecs.greenhousetozohopeoplesynchronize.repository.dwh.ZPEmployeeRepository;
import com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse.EmailSender;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RequiredArgsConstructor
@Service
@Log4j2
public class TerminationSyncService {
    private final ZPEmployeeRepository zpEmployeeRepository;
    private final ZOEmployeeRepository zoEmployeeRepository;

    @Autowired
    private ZOService zoService;

    private String normalizeMobile(String string) {
        String pattern1 = "(380+\\d{9})" +
                "|(\\+380+\\d{9})" +
                "|(0+\\d{9})" +
                "|(\\d{9})" +
                "|(\\d{3})-(\\d{3})-(\\d{2})-(\\d{2})" +
                "|(\\d{3})\\s\\((\\d{2})\\)\\s(\\d{3})-(\\d{4})" +
                "|(\\d{3})\\s(\\d{7})" +
                "|\\+(\\d{3})\\s\\((\\d{2})\\)\\s(\\d{3})-(\\d{4})" +
                "|(\\+380-\\d{2}-\\d{3}-\\d{2}-\\d{2})" +
                "|(380\\s\\d{2}\\s\\d{3}\\s\\d{2}\\s\\d{2})" +
                "|(\\(\\d{3}\\)\\s\\d{3}\\s\\d{2}\\s\\d{2})" +
                "|(\\d{3}-\\d{2}-\\d{2}-\\d{3})" +
                "|(\\d{3}-\\d{9})";
        Pattern pattern = Pattern.compile(pattern1);
        Matcher matcher = pattern.matcher(string);
        String number = "";
        if (matcher.matches()) {
            number = matcher.group().replaceAll("[^0-9]", "");
            if (number.length() == 9)
                number = "380" + number;
            if (number.length() == 10)
                number = "38" + number;
        }
        return number;
    }

    public void normalizeNumber() {
        List<ZPEmployee> zpEmployees = zpEmployeeRepository.getAllByMobileIsNotNull();
        zpEmployees.forEach(item -> {
            String number = normalizeMobile(item.getMobile());
            item.setNormalizeNumber(number);
        });

        zpEmployeeRepository.saveAll(zpEmployees);
        List<ZOEmployee> zoEmployees = zoEmployeeRepository.getAllByMobilePhoneIsNotNull();
        zoEmployees.forEach(item -> {
            String number = normalizeMobile(item.getMobilePhone());
            item.setNormalizeNumber(number);
        });
        zoEmployeeRepository.saveAll(zoEmployees);
    }


    public void synchronizeActiveEmployees() {
        normalizeNumber();

        List<ZPEmployee> zpEmployees = zpEmployeeRepository.findAll();
        List<ZOEmployee> zoEmployees = zoEmployeeRepository.findAll();


        List<String> zpPhones = new ArrayList<>();
        List<String> zoPhones = new ArrayList<>();

        List<ZPEmployee> zpEmployeesWithoutCorrectNumber = new ArrayList<>();
        List<ZOEmployee> zoEmployeesWithoutCorrectNumber = new ArrayList<>();


        zpEmployees.removeIf(item -> {
            String emailInLowerCase = item.getEmailID().toLowerCase();
            item.setEmailID(emailInLowerCase);
            if (item.getNormalizeNumber().equals("")) {
                zpEmployeesWithoutCorrectNumber.add(item);
                return true;
            } else {
                zpPhones.add(item.getNormalizeNumber());
                return false;
            }
        });


        zoEmployees.removeIf(item -> {
            if (item.getEmail() == null) {
                zoEmployeesWithoutCorrectNumber.add(item);
            } else {
                String emailInLowerCase = item.getEmail().toLowerCase();
                item.setEmail(emailInLowerCase);
            }
            if ((item.getNormalizeNumber() == null) || (item.getNormalizeNumber().equals(""))) {
                zoEmployeesWithoutCorrectNumber.add(item);
                return true;
            } else {
                zoPhones.add(item.getNormalizeNumber());
                return false;
            }
        });

        EmailSender.zpEmployeesWithoutCorrectNumber.addAll(zpEmployeesWithoutCorrectNumber);
        EmailSender.zoEmployeesWithoutCorrectNumber.addAll(zoEmployeesWithoutCorrectNumber);
        zpPhones.removeAll(zoPhones);

        Map<String, ZPEmployee> emailEmployeeMap = new HashMap<>();

        List<ZPEmployee> zpEmployeesWithProblem = zpEmployeeRepository.getAllByNormalizeNumberIn(zpPhones);

        for (ZPEmployee zpEmployee : zpEmployeesWithProblem) {
            emailEmployeeMap.put(zpEmployee.getEmailID().toLowerCase(), zpEmployee);
        }

        List<ZOEmployee> zoEmployeesWithWrongNumber = zoEmployeeRepository.getAllByEmailIn(emailEmployeeMap.keySet());
        zoEmployeesWithWrongNumber.forEach(item -> {
            emailEmployeeMap.remove(item.getEmail().toLowerCase());
        });

        List<ZPEmployee> zpEmployeesWithoutZO = new ArrayList<>(emailEmployeeMap.values());
        List<ZOEmployee> newZOEmployees = new ArrayList<>();

        for (ZPEmployee zp : zpEmployeesWithoutZO) {
            newZOEmployees.add(new ZOEmployee(zp.getFirstName() + " " + zp.getLastName(),
                    zp.getNormalizeNumber(),
                    zp.getEmailID().toLowerCase(),
                    zp.getEmployeestatus(),
                    (zp.getDateOfBirth() != null) ?
                            Date.valueOf(zp.getDateOfBirth()) : null));
        }

        for (ZOEmployee newZOEmployee : newZOEmployees) {
            System.out.println(newZOEmployee);
        }
        if (!newZOEmployees.isEmpty()) {
            //EmailSender.newEmployees.addAll(newZOEmployees);
            zoService.insertRecords(newZOEmployees);
        }

        log.info("success");
        for (ZOEmployee zoEmployee : zoEmployeesWithWrongNumber) {
            ZPEmployee zpEmployee = zpEmployeeRepository.getByEmailID(zoEmployee.getEmail().toLowerCase());
            zoEmployee.setMobilePhone(zpEmployee.getNormalizeNumber());
        }

        for (ZOEmployee zoEmployee : zoEmployeesWithWrongNumber) {
            System.out.println(zoEmployee);
        }

        zoService.updateRecords(zoEmployeesWithWrongNumber);
    }

    public String insertOneEmployee(EmployeeInsertDto dto) {
        List<ZOEmployee> employees = new ArrayList<>();
        employees.add(new ZOEmployee(dto.getName(), dto.getMobilePhone(), dto.getEmail(), "Newcomer", dto.getBirthdayDate()));
        log.info("user was created");
        zoService.insertRecords(employees);
        return "success";
    }
}


