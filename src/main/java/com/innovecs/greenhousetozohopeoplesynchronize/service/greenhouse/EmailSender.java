package com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse;

import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZOEmployee;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZPEmployee;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.relations.GHDepartment;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.relations.GHInterviewer;
import com.sun.mail.smtp.SMTPTransport;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

@Log4j2
@Component
@RequiredArgsConstructor
public class EmailSender {

    @Value("${mail.innovecs.username}")
    @NotNull
    private String USERNAME;

    @Value("${mail.innovecs.password}")
    @NotNull
    private String password;

    private static final String SMTP_SERVER = "smtp.office365.com";
    private static final String EMAIL_FROM = "integrity-checker@innovecs.com";

    public static List<ZPEmployee> zpEmployeesWithoutCorrectNumber = new ArrayList<>();
    public static List<ZOEmployee> zoEmployeesWithoutCorrectNumber = new ArrayList<>();
    public static List<ZOEmployee> employeesWithUpdatedStatus = new ArrayList<>();
    public static List<ZOEmployee> newEmployees =  new ArrayList<>();

    public void createLetterOfInvalidMobilePhone(){
        StringBuilder letter = new StringBuilder("Hello<br>");
        if(!zoEmployeesWithoutCorrectNumber.isEmpty()){
            letter.append("<br><b>Following employees don`t have the correct number in Zoho One:</b><br>");
            letter.append(zoEmployeesWithoutCorrectNumber.stream().map(ZOEmployee::getName).collect(Collectors.joining(";<br>")));
        }
        if(!zpEmployeesWithoutCorrectNumber.isEmpty()){
            letter.append("<br><b>Following employees don`t have the correct number in Zoho People:</b><br>");
            letter.append(zpEmployeesWithoutCorrectNumber.stream().map(item -> item.getFirstName() + " " + item.getLastName())
                    .collect(Collectors.joining(";<br>")));
        }
        if(!newEmployees.isEmpty()){
            letter.append("<br><b>Following employees are new in Zoho One:</b><br>");
            letter.append(newEmployees.stream().map(ZOEmployee::getName).collect(Collectors.joining(";<br>")));
        }

        if(!employeesWithUpdatedStatus.isEmpty()){
            letter.append("<br><b>Following employees are new in Zoho One:</b><br>");
            letter.append(employeesWithUpdatedStatus.stream().map(item -> item.getName() + ", " + item.getStatus())
                    .collect(Collectors.joining(";<br>")));
        }

        letter.append("<br>Have a nice day");
        sendMail("Zoho One sync", letter.toString());
        employeesWithUpdatedStatus = new ArrayList<>();
        zoEmployeesWithoutCorrectNumber = new ArrayList<>();
        zpEmployeesWithoutCorrectNumber = new ArrayList<>();
        newEmployees = new ArrayList<>();
    }


    public void createLetterOfDeletedDepartments(List<GHDepartment> ghDeletedDepartmentList,
                                                 List<GHDepartment> ghNonexistentDepartmentList) {
        StringBuilder letter = new StringBuilder("Hello<br>");
        if (!ghDeletedDepartmentList.isEmpty()) {
            letter.append("<br><b>Following departments have been deleted from Zoho People.</b><br>");
            letter.append(ghDeletedDepartmentList.stream().map(GHDepartment::getDepartment).collect(Collectors.joining("; <br>"))).append("<br>Best wishes,<br>Corporate Systems");
        }
        if (!ghNonexistentDepartmentList.isEmpty()) {
            letter.append("<br><b>Following departments exist only in Green House System.</b><br>");
            letter.append(ghNonexistentDepartmentList.stream().map(GHDepartment::getName).collect(Collectors.joining("; <br>"))).append("<br>Best wishes,<br>Corporate Systems");
        }

        letter.append("<br><br>Please be aware of it and have a nice day");
        sendMail("Departments Mismatches between ZohoPeople and GreenHouse",letter.toString());
    }

    public void createLetterOfUnknownUsers(List<GHInterviewer> unknownUsers,
                                           List<GHInterviewer> unmarkedEmployees) {
        StringBuilder letter = new StringBuilder("Hello <br><br>");
        if (!unknownUsers.isEmpty()) {
            letter.append("<b>Following users have no record at ZP or have with another mail:</b> <br>");
            letter.append(unknownUsers.stream().map(GHInterviewer::getName).collect(Collectors.joining("; <br>")));
            log.info("records of unknown users have been written");
        }
        if (!unmarkedEmployees.isEmpty()) {
            letter.append("<br><br><b>Following employees are users in Green House System, " +
                    "but they aren't marked as GH Interviewer.</b><br>");
            for (GHInterviewer unmarkedEmployee : unmarkedEmployees) {
                letter.append("<a href=\"https://people.zoho.eu/myinnovecs/zp#selfservice/user/profile-id:" + unmarkedEmployee.getZohoID() + "\">")
                        .append(unmarkedEmployee.getName())
                        .append("</a><br>");
            }
        }
        letter.append("<br><br>Please be aware of it and have a nice day");

        sendMail("Users Mismatches between GreenHouse and ZohoPeople", letter.toString());
    }

    public void sendMail(String subject, String letter) {

        Properties prop = System.getProperties();
        prop.put("mail.smtp.host", SMTP_SERVER);
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.starttls.enable", "true");

        Session session = Session.getInstance(prop, null);
        Message msg = new MimeMessage(session);

        try {

            msg.setFrom(new InternetAddress(EMAIL_FROM));
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse("corpsys@innovecs.com", false));
//                    InternetAddress.parse("tetiana.sharovetska@innovecs.com", false));

            msg.setSubject(subject);

            msg.setContent(letter, "text/html; charset=utf-8");
            SMTPTransport t = (SMTPTransport) session.getTransport("smtp");
            t.connect(SMTP_SERVER, USERNAME, password);
            t.sendMessage(msg, msg.getAllRecipients());
            log.info("Letter is sent");
            t.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
