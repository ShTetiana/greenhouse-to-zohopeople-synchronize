package com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse.synchronizers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZPEmployee;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.relations.GHDepartment;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.relations.GHInterviewer;
import com.innovecs.greenhousetozohopeoplesynchronize.repository.dwh.ZPEmployeeRepository;
import com.innovecs.greenhousetozohopeoplesynchronize.repository.relations.GHDepartmentRepository;
import com.innovecs.greenhousetozohopeoplesynchronize.repository.relations.GHInterviewerRepository;
import com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse.EmailSender;
import com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse.GHRequestService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;

@Data
@Log4j2
@Service
@EnableScheduling
@RequiredArgsConstructor
public class InterviewerSyncService {

    private final RestTemplate restTemplate;
    private final HttpEntity httpEntity;
    private final ObjectMapper objectMapper;

    private final ZPEmployeeRepository zpEmployeeRepository;
    private final GHInterviewerRepository ghInterviewerRepository;
    private final GHDepartmentRepository ghDepartmentRepository;

    private final GHRequestService ghRequestService;
    private final EmailSender emailSender;

    private final static List<String> ACTIVE_INNOVECS_STATUSES = Arrays.asList("Employee", "Newcomer", "Termination obligations");
    private final static List<String> INACTIVE_INNOVECS_STATUSES = Arrays.asList("Ready to start", "Terminated");

    private List<GHInterviewer> getGHUsers() {
        ResponseEntity<String> response = null;
        int counter = 1;

        List<GHInterviewer> ghInterviewerList = new ArrayList<>();

        try {
            while (true) {
                String url = "https://harvest.greenhouse.io/v1/users?page=" + counter + "&per_page=500";

                response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);

                JsonNode parentNode = objectMapper.readTree(Objects.requireNonNull(response.getBody()));

                if (parentNode.isArray() && parentNode.size() > 0) {
                    log.info(String.format("GH user size: %d, counter: %d ", parentNode.size(), counter));

                    List<GHInterviewer> pojos = objectMapper.convertValue(
                            parentNode,
                            new TypeReference<List<GHInterviewer>>() {
                            });
//                    ghInterviewerRepository.saveAll(pojos);
                    ghInterviewerList.addAll(pojos);
                } else {
                    log.info("reached maximum GH user, size: " + ghInterviewerRepository.count());
                    break;
                }
                counter++;
            }

        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return ghInterviewerList;
    }

    public void updateGHInterviewerRelationsBeforeSync() {
        ghInterviewerRepository.deleteAll();
        Map<String, ZPEmployee> mailZpEmployeeMap = zpEmployeeRepository.findAllByEmailIDIsNot("").stream().
                collect(Collectors.toMap(item -> item.getEmailID().toLowerCase(), item -> item));

        List<GHInterviewer> ghInterviewerList = getGHUsers();
        for (GHInterviewer ghInterviewer : ghInterviewerList) {
            ghInterviewer.setPrimaryEmailAddress(ghInterviewer.getPrimaryEmailAddress().toLowerCase());
        }

        int counter = 0;

        for (GHInterviewer ghInterviewer : ghInterviewerList) {
            if (mailZpEmployeeMap.containsKey(ghInterviewer.getPrimaryEmailAddress())) {
                ZPEmployee employee = mailZpEmployeeMap.get(ghInterviewer.getPrimaryEmailAddress());
                ghInterviewer.setInnovecsStatus(employee.getInnovecsStatus1());
                ghInterviewer.setZohoID(employee.getZohoID());
                counter++;
            }
        }
        log.info("Size of connected users: " + counter);

        log.info("Size of saved users: " + ghInterviewerList.size());
        ghInterviewerRepository.saveAll(ghInterviewerList);
        log.info("finish of updating");
    }


    @Scheduled(cron = "${sync.interviewer.cron}")
    public void sync() {
        updateGHInterviewerRelationsBeforeSync();
        syncTalentAcquisitionDepartment();
        syncMarkedZPEmployees();
        updateGHInterviewerRelationsAfterSync();
    }

    public void syncTalentAcquisitionDepartment() {
        log.info("start of sync of talent acquisition department");
        try {
            Optional<GHDepartment> optionalTalentAcquisitionDepartment = ghDepartmentRepository.findByDepartment("Talent Acquisition");
            if (optionalTalentAcquisitionDepartment.isPresent()) {
                GHDepartment ghDepartment = optionalTalentAcquisitionDepartment.get();
                List<ZPEmployee> zpEmployees = zpEmployeeRepository.findAllByDepartmentIDAndInnovecsStatus1In(
                        ghDepartment.getZohoID(), ACTIVE_INNOVECS_STATUSES);
                newEmployeeFlow(zpEmployees);

                List<ZPEmployee> terminatedEmployees = zpEmployeeRepository.findAllByDepartmentIDAndInnovecsStatus1In(
                        ghDepartment.getZohoID(), INACTIVE_INNOVECS_STATUSES);
                for (ZPEmployee employee : terminatedEmployees) {
                    terminatedEmployeeFlow(employee);
                }
            } else {
                log.error("Talent Acquisition department has not been found");
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
        log.info("finish of sync of talent acquisition department");
    }

    public void syncMarkedZPEmployees() {
        log.info("start of marked employees synchronization");
        List<ZPEmployee> zpEmployees = zpEmployeeRepository.findAllByGhInterviewerAndInnovecsStatus1InAndDepartmentIsNot(
                true, ACTIVE_INNOVECS_STATUSES, "Talent Acquisition");
        newEmployeeFlow(zpEmployees);

        List<ZPEmployee> zpTerminatedEmployees = zpEmployeeRepository.findAllByGhInterviewerAndInnovecsStatus1InAndDepartmentIsNot(
                true, INACTIVE_INNOVECS_STATUSES, "Talent Acquisition");
        for (ZPEmployee zpTerminatedEmployee : zpTerminatedEmployees) {
            terminatedEmployeeFlow(zpTerminatedEmployee);
        }
        log.info("finish of marked employees sync");
    }

    private void enableExistedUser(GHInterviewer ghInterviewer, ZPEmployee zpEmployee) {
        if (ghInterviewer.getZohoID() == null) {
            ghInterviewer.setZohoID(zpEmployee.getZohoID());
        }
        ghInterviewer.setInnovecsStatus(zpEmployee.getInnovecsStatus1());
        ghInterviewer.setDisabled(false);
        sendEnableUserRequest(ghInterviewer);
    }

    private void sendDisableUserRequest(GHInterviewer ghInterviewer) {
        log.info("sending disable request for " + ghInterviewer.getPrimaryEmailAddress());
        String body = "{ \"user\" : { \"user_id\": " + ghInterviewer.getId() + " } }";
        ghRequestService.sendUpdateRequest(
                "https://harvest.greenhouse.io/v2/users/disable",
                body);
    }

    private void sendEnableUserRequest(GHInterviewer ghInterviewer) {
        log.info("sending disable request for " + ghInterviewer.getPrimaryEmailAddress());
        String body = "{ \"user\" : { \"user_id\": " + ghInterviewer.getId() + " } }";
        System.out.println(body);
        ghRequestService.sendUpdateRequest(
                "https://harvest.greenhouse.io/v2/users/enable",
                body);
    }

    private void sendAddUserRequest(GHInterviewer ghInterviewer) {
        try {
            StringBuilder body = new StringBuilder("{")
                    .append(" \"first_name\": \"" + ghInterviewer.getFirstName() + "\", ")
                    .append(" \"last_name\": \"" + ghInterviewer.getLastName() + "\", ")
                    .append(" \"email\": \"" + ghInterviewer.getPrimaryEmailAddress() + "\", ")
                    .append(" \"send_email_invite\": false } ");
            System.out.println(body.toString());// change this fields on true if notification letter will be required
            ghRequestService.sendPostRequest("https://harvest.greenhouse.io/v1/users",
                    body.toString());
            Thread.sleep(2000);
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
    }

    private void newEmployeeFlow(List<ZPEmployee> zpEmployees) {
        Map<String, GHInterviewer> ghInterviewerMap = ghInterviewerRepository.findAll().stream()
                .collect(Collectors.toMap(GHInterviewer::getPrimaryEmailAddress, item -> item));

        Map<Long, GHInterviewer> zohoidGHInterviewerMap = ghInterviewerRepository.getAllByZohoIDIsNotNull().stream()
                .collect(Collectors.toMap(GHInterviewer::getZohoID, item -> item));

        // sync of department account
        int[] counters = {0, 0}; // first it is the employees who already have gh acc, second is for fully new people
        for (ZPEmployee zpEmployee : zpEmployees) {
            if (ghInterviewerMap.containsKey(zpEmployee.getEmailID())
                    && ghInterviewerMap.get(zpEmployee.getEmailID()).getDisabled()) {
                enableExistedUser(ghInterviewerMap.get(zpEmployee.getEmailID()), zpEmployee);
                counters[0]++;
            } else {
                //check zoho id
                if (zohoidGHInterviewerMap.containsKey(zpEmployee.getZohoID())
                        && zohoidGHInterviewerMap.get(zpEmployee.getZohoID()).getDisabled()) {
                    enableExistedUser(zohoidGHInterviewerMap.get(zpEmployee.getZohoID()), zpEmployee);
                    counters[0]++;
                } else if (!zohoidGHInterviewerMap.containsKey(zpEmployee.getZohoID())) {
                    GHInterviewer ghInterviewer = new GHInterviewer(zpEmployee);
                    ghInterviewerRepository.save(ghInterviewer);
                    sendAddUserRequest(ghInterviewer);
                    counters[1]++;
                }
            }
        }
        ghInterviewerRepository.saveAll(ghInterviewerMap.values());
        log.info("Green House accounts of " + counters[0] + " users has been enabled");
        log.info("Green House accounts has been created for " + counters[1] + " new employees");

    }

    private void terminatedEmployeeFlow(ZPEmployee zpEmployee) {
        Optional<GHInterviewer> optionalGHInterviewer = ghInterviewerRepository.getByZohoID(zpEmployee.getZohoID());
        if (optionalGHInterviewer.isPresent()) {
            GHInterviewer ghInterviewer = optionalGHInterviewer.get();
            if (!ghInterviewer.getDisabled()) {
                ghInterviewer.setDisabled(true);
                ghInterviewerRepository.save(ghInterviewer);
                sendDisableUserRequest(ghInterviewer);
                log.info("Employee " + zpEmployee.getEmailID() + " has been disabled");
            }
        } else {
            log.info("Employee " + zpEmployee.getEmailID() + " has no gh account");
        }
    }

    private void updateGHInterviewerRelationsAfterSync() {
        ghInterviewerRepository.deleteAll();
        Map<String, ZPEmployee> mailZpEmployeeMap = zpEmployeeRepository.findAllByEmailIDIsNot("").stream().
                collect(Collectors.toMap(item -> item.getEmailID().toLowerCase(), item -> item));

        List<GHInterviewer> ghInterviewerList = getGHUsers();
        for (GHInterviewer ghInterviewer : ghInterviewerList) {
            ghInterviewer.setPrimaryEmailAddress(ghInterviewer.getPrimaryEmailAddress().toLowerCase());
        }

        List<GHInterviewer> unknownUsers = new ArrayList<>();
        List<GHInterviewer> unmarkedEmployees = new ArrayList<>();

        int counter = 0;

        for (GHInterviewer ghInterviewer : ghInterviewerList) {
            if (mailZpEmployeeMap.containsKey(ghInterviewer.getPrimaryEmailAddress())) {
                ZPEmployee employee = mailZpEmployeeMap.get(ghInterviewer.getPrimaryEmailAddress());
                ghInterviewer.setInnovecsStatus(employee.getInnovecsStatus1());
                ghInterviewer.setZohoID(employee.getZohoID());
                counter++;
                if (!employee.getGhInterviewer()) {
                    unmarkedEmployees.add(ghInterviewer);
                }
            } else if (!ghInterviewer.getDisabled()) {
                unknownUsers.add(ghInterviewer);
            }
        }
        log.info("Size of connected users: " + counter);
        log.info("Size of unknown employees: " + unknownUsers.size() + ", size of unmarked users: " + unmarkedEmployees.size());
        if (!unknownUsers.isEmpty() || !unmarkedEmployees.isEmpty()) {
            emailSender.createLetterOfUnknownUsers(unknownUsers, unmarkedEmployees);
        }

        log.info("Size of saved users: " + ghInterviewerList.size());
        ghInterviewerRepository.saveAll(ghInterviewerList);
        log.info("finish of updating");

    }
}
