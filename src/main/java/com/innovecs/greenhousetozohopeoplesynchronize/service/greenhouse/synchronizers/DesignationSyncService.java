package com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse.synchronizers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.auxiliary.GHCustomFieldOption;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZPDesignation;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.relations.ZPDesignationGHTitleRelation;
import com.innovecs.greenhousetozohopeoplesynchronize.repository.dwh.ZPDesignationRepository;
import com.innovecs.greenhousetozohopeoplesynchronize.repository.relations.ZPDesignationGHTitleRelationRepository;
import com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse.DwhForeignKeyManager;
import com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse.GHRequestService;
import com.squareup.okhttp.OkHttpClient;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Data
@Log4j2
@Service
@EnableScheduling
@RequiredArgsConstructor
public class DesignationSyncService {

    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;
    private final HttpEntity<Object> httpEntity;
    private final ZPDesignationRepository zpDesignationRepository;
    private final ZPDesignationGHTitleRelationRepository zpDesignationGHTitleRelationRepository;
    private static Timestamp previousCheckTime;
    private final OkHttpClient okHttpClient;
    private final DwhForeignKeyManager dwhForeignKeyManager;

    private final GHRequestService ghRequestService;

//    @Scheduled(cron = "0 0/15 18-20 ? * MON-FRI")
//    public void latterSync() {
//        sync();
//    }

//    @Scheduled(cron = "0 38/5 9-17 ? * Mon-fri")
    @Scheduled(cron = "0 6 6 ? * Mon-fri")
    public void sync() {
        try {
            LocalDate localDate = LocalDate.parse("2020-03-01");
            Timestamp timestamp = Timestamp.valueOf(localDate.atTime(LocalTime.MIDNIGHT));

            log.info(previousCheckTime);
            Timestamp fixTime = (previousCheckTime != null) ?
                    previousCheckTime : timestamp;
            // in case when the system has been restarted, all record modified since March 1, 2020 will be checked
            log.info("sync is started. Fix time: " + fixTime);
            List<ZPDesignation> zpCurrentDesignations = getCurrentDesignations();
            if (!zpCurrentDesignations.isEmpty()) {

                Map<Long, ZPDesignationGHTitleRelation> zpDesignationGHTitleRelationMap = zpDesignationGHTitleRelationRepository.findAll()
                        .stream().collect(Collectors.toMap(ZPDesignationGHTitleRelation::getZohoID, item -> item));


                // deleted relations block
                List<ZPDesignationGHTitleRelation> deletedRelations = new ArrayList<>();
                List<Long> zpCurrentDesignationZohoIdList = zpCurrentDesignations.stream().map(ZPDesignation::getZohoID).collect(Collectors.toList());
                zpDesignationGHTitleRelationMap.forEach((zohoId, relation) -> {
                    if (!zpCurrentDesignationZohoIdList.contains(zohoId)) {
                        deletedRelations.add(relation);
                    }
                });

                if (!deletedRelations.isEmpty()) {
                    sendDeleteRequest(deletedRelations);
                    zpDesignationGHTitleRelationRepository.deleteAll(deletedRelations);

                    List<ZPDesignation> zpDeletedDesignations = zpDesignationRepository.getAllByZohoIDIn(
                            deletedRelations.stream().map(ZPDesignationGHTitleRelation::getZohoID).collect(Collectors.toList()));
                    dwhForeignKeyManager.dropZPDesignationForeignKeys();
                    zpDesignationRepository.deleteAll(zpDeletedDesignations);
                    dwhForeignKeyManager.addZPDesignationForeignKeys();
                } else {
                    log.info("List of the deleted ZP Designations is empty");
                }

                Integer priorityCounter = zpDesignationGHTitleRelationMap.values().stream().map(ZPDesignationGHTitleRelation::getPriority).max(Integer::compareTo).get();

                zpCurrentDesignations.removeIf(zpDesignation -> zpDesignation.getModifiedTime().before(fixTime));

                List<ZPDesignationGHTitleRelation> zpNewRelations = new ArrayList<>();
                List<ZPDesignationGHTitleRelation> zpUpdatedRelations = new ArrayList<>();
                List<ZPDesignation> zpNewDesignation = new ArrayList<>();

                for (ZPDesignation designation : zpCurrentDesignations) {
                    if (zpDesignationGHTitleRelationMap.containsKey(designation.getZohoID())
                            && !zpDesignationGHTitleRelationMap.get(designation.getZohoID()).getName()
                            .equals(designation.getDesignation())) {
                        ZPDesignationGHTitleRelation relation = zpDesignationGHTitleRelationMap.get(designation.getZohoID());
                        relation.setName(designation.getDesignation());
                        zpUpdatedRelations.add(relation);
                    } else if (!zpDesignationGHTitleRelationMap.containsKey(designation.getZohoID())) {
                        zpNewRelations.add(
                                new ZPDesignationGHTitleRelation(designation.getZohoID(),
                                        designation.getDesignation(), (++priorityCounter)));
                        zpNewDesignation.add(designation);
                    }
                }


                // new Relations block
                if (!zpNewRelations.isEmpty()) {
                    log.info("Size of New Designations: " + zpNewRelations.size());
                    sendPostRequest(zpNewRelations);
                    Map<String, GHCustomFieldOption> ghTitleMap = getListOfCurrentGHTitles().stream()
                            .collect(Collectors.toMap(GHCustomFieldOption::getName, ghCustomFieldOption -> ghCustomFieldOption));
                    for (ZPDesignationGHTitleRelation zpNewRelation : zpNewRelations) {
                        if (ghTitleMap.containsKey(zpNewRelation.getName())) {
                            zpNewRelation.setGhID(ghTitleMap.get(zpNewRelation.getName()).getId());
                        } else {
                            log.warn("Title: " + zpNewRelation.getName() + " has not been added to Green House");
                        }
                    }
                    log.info("ready to save all new Designation -> Titles to database");
                    zpDesignationGHTitleRelationRepository.saveAll(zpNewRelations);
                    zpDesignationRepository.saveAll(zpNewDesignation);
                } else {
                    log.info("There is no new designation");
                }

                // updated relations block
                if (!zpUpdatedRelations.isEmpty()) {
                    log.info("Size of Updated Designations: " + zpUpdatedRelations.size());
                    sendUpdateRequest(zpUpdatedRelations);

                    log.info("ready to save all updated Designation -> Titles to database");
                    zpDesignationGHTitleRelationRepository.saveAll(zpUpdatedRelations);
                    List<ZPDesignation> zpUpdatedDesignations = zpDesignationRepository.getAllByZohoIDIn(
                            zpUpdatedRelations.stream().map(ZPDesignationGHTitleRelation::getZohoID).collect(Collectors.toList()));
                    Map<Long, ZPDesignationGHTitleRelation> zpRelationMap = zpUpdatedRelations.stream()
                            .collect(Collectors.toMap(ZPDesignationGHTitleRelation::getZohoID, item -> item));
                    for (ZPDesignation zpUpdatedDesignation : zpUpdatedDesignations) {
                        zpUpdatedDesignation.setDesignation(
                                zpRelationMap.get(zpUpdatedDesignation.getZohoID()).getName());
                    }
                    zpDesignationRepository.saveAll(zpUpdatedDesignations);
                }

                log.info("sync  between ZP Designation And GH Titles is finished");

                previousCheckTime = fixTime;
            }
            else log.warn("ZP didn't give any record");
        } catch (Exception ex) {
            log.info(ex.getMessage());
        }
    }


    public List<GHCustomFieldOption> getListOfCurrentGHTitles() {
        List<GHCustomFieldOption> ghCustomFieldOptions = new ArrayList<>();

        ResponseEntity<String> response = null;

        try {
            String url = "https://harvest.greenhouse.io/v1/custom_field/8895385002";
            response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
            JsonNode parentNode = objectMapper.readTree(Objects.requireNonNull(response.getBody())).get("custom_field_options");
            if (parentNode.isArray() && parentNode.size() > 0) {
                log.info("GH titles size:" + parentNode.size());
                ghCustomFieldOptions = objectMapper.convertValue(
                        parentNode,
                        new TypeReference<List<GHCustomFieldOption>>() {
                        });
            }
            Thread.sleep(1100);
        } catch (Exception e) {
            log.error("Error description", e);
        }
        return ghCustomFieldOptions;
    }

    public void fillInRelationTable() {
        List<ZPDesignation> zpDesignationList = zpDesignationRepository.findAll();
        List<GHCustomFieldOption> ghCustomFieldOptionList = getListOfCurrentGHTitles();
        Map<String, GHCustomFieldOption> ghTitleMap = ghCustomFieldOptionList.stream()
                .collect(Collectors.toMap(GHCustomFieldOption::getName, item -> item));

        List<ZPDesignationGHTitleRelation> relationList = new ArrayList<>();
        int i = 0;

        for (ZPDesignation zpDesignation : zpDesignationList) {
            if (ghTitleMap.containsKey(zpDesignation.getDesignation())) {
                ZPDesignationGHTitleRelation relation = new ZPDesignationGHTitleRelation();
                relation.setZohoID(zpDesignation.getZohoID());
                relation.setGhID(ghTitleMap.get(zpDesignation.getDesignation()).getId());
                relation.setName(zpDesignation.getDesignation());
                relation.setPriority(ghTitleMap.get(zpDesignation.getDesignation()).getPriority());
                relationList.add(relation);
                i++;
            }
        }
        zpDesignationGHTitleRelationRepository.saveAll(relationList);
        log.info("Size gh: " + ghCustomFieldOptionList.size() + "\n zp: " + zpDesignationList.size()
                + "sync:" + i);
    }

    private List<ZPDesignation> getCurrentDesignations() {
        List<ZPDesignation> zpDesignations =  zpDesignationRepository.findAll().stream().filter(
        item -> !item.getDesignation().contains("[8ac5a1]")).collect(Collectors.toList());
        log.info(zpDesignations.size());
        return zpDesignations;
    }

    public void sendUpdateRequest(List<ZPDesignationGHTitleRelation> list) {
        try {
            //{  "options": [  {  "id": 17040844002,   "name": "Option A"}  ]}
            StringBuilder stringBuilder = new StringBuilder("{\"options\":[");
            for (ZPDesignationGHTitleRelation relation : list) {
                stringBuilder.append("{\"id\": " + relation.getGhID() + ", \"name\": \"" + relation.getName() + "\"},");
            }
            stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(",")).append(" ]}");

            ghRequestService.sendUpdateRequest(
                    "https://harvest.greenhouse.io/v1/custom_field/8895385002/custom_field_options",
                    stringBuilder.toString());
            //
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
    }

    public void sendPostRequest(List<ZPDesignationGHTitleRelation> list) {
        try {
            // {"name": "Option A", "priority": 137},
            StringBuilder stringBuilder = new StringBuilder("{\"options\":[");
            for (ZPDesignationGHTitleRelation relation : list) {
                stringBuilder.append("{\"name\": \"" + relation.getName() + "\", \"priority\": " + relation.getPriority() + "},");
            }
            stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(",")).append(" ]}");
            ghRequestService.sendPostRequest(
                    "https://harvest.greenhouse.io/v1/custom_field/8895385002/custom_field_options",
                    stringBuilder.toString());
        } catch (Exception exception) {
            log.error(exception.getMessage());
        }
    }

    public void sendDeleteRequest(List<ZPDesignationGHTitleRelation> relationList) {
        try {
            String requestBody = "{ \"option_ids\": [" +
                    relationList.stream().map(item -> item.getGhID().toString()).collect(Collectors.joining(", ")) + " ] }";
            ghRequestService.sendDeleteRequest(
                    "https://harvest.greenhouse.io/v1/custom_field/8895385002/custom_field_options",
                    requestBody);
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
    }

    private void updateJobsWithNewSalaryCrd(List<ZPDesignationGHTitleRelation> list){


    }


}



