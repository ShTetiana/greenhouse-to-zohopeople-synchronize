package com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.stereotype.Service;

import java.sql.Connection;

@Log4j2
@Service
@RequiredArgsConstructor
public class DwhForeignKeyManager {


    private final JdbcTemplate dwhCrawlerJdbcTemplate;


    @Value("classpath:sql/add_zp_department_related_foreign_keys.sql")
    private Resource addZPDepartmentConstraints;

    @Value("classpath:sql/drop_zp_department_related_foreign_keys.sql")
    private Resource dropDepartmentConstraints;

    @Value("classpath:sql/add_zp_designation_related_foreign_keys.sql")
    private Resource addZPDesignationConstraints;

    @Value("classpath:sql/drop_zp_department_related_foreign_keys.sql")
    private Resource dropDesignationConstraints;

    public void addZPDepartmentForeignKeys() {
        log.info("foreign keys of zp departments are set");
        try (Connection connection = dwhCrawlerJdbcTemplate.getDataSource().getConnection()) {
            ScriptUtils.executeSqlScript(connection, addZPDepartmentConstraints);
            log.info("foreign  keys were set");
        } catch (Exception e) {
            log.error("Attempt to set foreign keys failed" + e.getMessage());
        }
    }

    public void dropZPDepartmentForeignKeys() {
        log.info("foreign keys of zp departments are dropped");
        try (Connection connection = dwhCrawlerJdbcTemplate.getDataSource().getConnection()) {
            ScriptUtils.executeSqlScript(connection, dropDepartmentConstraints);
            log.info("foreign  keys were dropped");
        } catch (Exception e) {
            log.error("Attempt to drop foreign keys failed" + e.getMessage());
        }
    }

    public void addZPDesignationForeignKeys() {
        log.info("foreign keys of zp designations are set");
        try (Connection connection = dwhCrawlerJdbcTemplate.getDataSource().getConnection()) {
            ScriptUtils.executeSqlScript(connection, addZPDesignationConstraints);
            log.info("foreign  keys were set");
        } catch (Exception e) {
            log.error("Attempt to set foreign keys failed" + e.getMessage());
        }
    }

    public void dropZPDesignationForeignKeys() {
        log.info("foreign keys of zp designations are dropped");
        try (Connection connection = dwhCrawlerJdbcTemplate.getDataSource().getConnection()) {
            ScriptUtils.executeSqlScript(connection, dropDesignationConstraints);
            log.info("foreign  keys were dropped");
        } catch (Exception e) {
            log.error("Attempt to drop foreign keys failed" + e.getMessage());
        }
    }


}
