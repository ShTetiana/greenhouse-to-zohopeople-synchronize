package com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@RequiredArgsConstructor
@Data
@Component
@Log4j2
public class GHRequestService {

    private final OkHttpClient okHttpClient;

    @Value("${innovecs.green-house.token}")
    private String token;

    @Value("${innovecs.green-house.admin-id}")
    private String adminId;

    //post request
    public void sendPostRequest(String url, String stringBody) {
        try {
            // {"name": "Option A", "priority": 137},;
            RequestBody body = RequestBody.create(com.squareup.okhttp.MediaType.parse("application/javascript"), stringBody);
            Request request = new Request.Builder()
                    .url(url)
                    .method("POST", body)
                    .addHeader("On-Behalf-Of", adminId)
                    .addHeader("Authorization", token)
                    .addHeader("Content-Type", "application/json")
                    .build();
            Response response = okHttpClient.newCall(request).execute();
            log.info("Post response: " + response.toString());
            response.body().close();
        } catch (Exception exception) {
            log.error(exception.getMessage());
        }
    }

    //update request
    public void sendUpdateRequest(String url, String requestBody) {
        try {
            RequestBody body = RequestBody.create(com.squareup.okhttp.MediaType.parse("application/javascript"),
                    requestBody);
            Request request = new Request.Builder()
                    .url(url)
                    .method("PATCH", body)
                    .addHeader("On-Behalf-Of", adminId)
                    .addHeader("Authorization", token)
                    .addHeader("Content-Type", "application/json")
                    .build();
            Response response = okHttpClient.newCall(request).execute();
            log.info("Update responce: " + response.toString());
            response.body().close();
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }

    }

    //delete request
    public void sendDeleteRequest(String url, String requestBody) {
        try {
            RequestBody body = RequestBody.create(com.squareup.okhttp.MediaType.parse("application/javascript"), requestBody);
            Request request = new Request.Builder()
                    .url(url)
                    .method("DELETE", body)
                    .addHeader("On-Behalf-Of", adminId)
                    .addHeader("Authorization", token)
                    .addHeader("Content-Type", "application/json")
                    .build();
            Response response = okHttpClient.newCall(request).execute();
            log.info("Delete responce: " + response.toString());
            response.body().close();
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
    }

}