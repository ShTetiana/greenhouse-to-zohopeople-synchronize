package com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse;


import com.fasterxml.jackson.databind.JsonNode;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZPDesignation;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZPLocation;
import com.innovecs.greenhousetozohopeoplesynchronize.repository.dwh.ZPDesignationRepository;
import com.innovecs.greenhousetozohopeoplesynchronize.repository.dwh.ZPLocationRepository;
import com.innovecs.greenhousetozohopeoplesynchronize.repository.dwh.ZPSeniorityRepository;
import com.innovecs.greenhousetozohopeoplesynchronize.repository.relations.GHDepartmentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.math3.util.Precision;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Log4j2
@Service
@EnableScheduling
@RequiredArgsConstructor
public class SalaryCardService {

    private static Integer maxSalaryRangeDayNumber;
    private static String minSalaryRangeDayName;

    private final JdbcTemplate jdbcTemplate;
    public static Map<Long, Timestamp> todayUpdatedJobs = new HashMap<>();

    private final ZPLocationRepository zpLocationRepository;
    private final ZPDesignationRepository zpDesignationRepository;
    private final GHRequestService requestService;
    private final GHDepartmentRepository ghDepartmentRepository;
    private final ZPSeniorityRepository zpSeniorityRepository;

    public void saveWebHook(JsonNode body) {
        try {
            if (maxSalaryRangeDayNumber == null) setSalaryRangeFields();
            JsonNode jobNode = body.get("payload").get("job");
            if (jobNode.get("custom_fields").get("designation").get("value") != null) {
                String title = jobNode.get("custom_fields").get("designation").get("value").textValue();

                if (todayUpdatedJobs.containsKey(jobNode.get("id").longValue())) {
                    long diff = new Timestamp(System.currentTimeMillis()).getTime() - todayUpdatedJobs.get(jobNode.get("id").longValue()).getTime();
                    if (diff < 30000) {
                        log.info("It is already updated in last 30 sec ");
                        return;
                    }
                }

                Optional<ZPDesignation> zpDesignationOptional = zpDesignationRepository.findByDesignation(title);
                if (zpDesignationOptional.isPresent()) {
                    ZPDesignation zpDesignation = zpDesignationOptional.get();
                    log.info(zpDesignation.getDesignation());
                    Object maxValue = zpDesignation.getClass().getField("max" + maxSalaryRangeDayNumber).get(zpDesignation);

                    if (maxValue == null) {
                        int range = maxSalaryRangeDayNumber - 1;
                        maxValue = zpDesignation.getClass().getField("max" + range).get(zpDesignation);
                    }

                    String location = (jobNode.get("offices").size() > 1) ?
                            "Multiple locations" :
                            jobNode.get("offices").get(0).get("name").textValue();

                    Optional<ZPLocation> zpLocationOptional = zpLocationRepository.findByCity(location);

                    Double max = 0.0;
                    Double currentReference = 0.0;

                    if (maxValue != null) {
                        max = (double) maxValue;
                        if (zpLocationOptional.isPresent()) {
                            max = Precision.round((max * zpLocationOptional.get().getSalaryCoefficient()), 2);
                        }
                    }

                    if (zpDesignation.getCurrentReference() != null && !zpDesignation.getCurrentReference().equals("")) {
                        currentReference = Double.parseDouble(zpDesignation.getCurrentReference());
                        if (zpLocationOptional.isPresent()) {
                            currentReference = Precision.round((currentReference * zpLocationOptional.get().getSalaryCoefficient()), 2);
                        }
                    }
                    LocalDate targetDate = null;
                    Integer sla = null;
                    try {
                       // System.out.println(body);
                        long zohoDepartmentId = jobNode.get("departments").get(0).get("id").longValue();
                        sla = jdbcTemplate.queryForObject("select division from zp_department zp left join zp_gh_department gh on zp.zohoid = gh.zohoid where gh.id = ?", new Object[]{zohoDepartmentId}, String.class ).equals("Delivery")?
                                zpSeniorityRepository.findBySeniorityIgnoreCase(zpDesignation.getSeniority()).get().getSLAForHiringCalDays() :
                                zpSeniorityRepository.findBySeniorityIgnoreCase(zpDesignation.getSeniority()).get().getSLAForHiringCalDaysInternal();

                        targetDate = LocalDateTime.parse(jobNode.get("opened_at").textValue(), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")).plusDays(sla).toLocalDate();

                    } catch (Exception ex) {
                        log.warn("department has not been found", ex.getMessage());
                        ex.printStackTrace();
                    }
                    log.info("Data is ready, max: " + max + ", cr: " + currentReference + ", sla: "+ sla);
                    sendRequest(max, currentReference, jobNode.get("id").longValue(),targetDate, sla);
                    todayUpdatedJobs.put(jobNode.get("id").longValue(), new Timestamp(System.currentTimeMillis()));

                }
            } else {
                log.info("Job without title: " + body.get("payload").get("job").get("id"));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error(ex.getMessage());
        }
    }

    public void setSalaryRangeFields() {
        int maxNumber = 1;
        Pattern pattern = Pattern.compile("max(\\d*)");
        Class<ZPDesignation> carClass = ZPDesignation.class;
        Field[] declaredFields = carClass.getDeclaredFields();
        for (Field field : declaredFields) {
            Matcher matcher = pattern.matcher(field.getName());
            if (matcher.find()) {
                if (Integer.parseInt(matcher.group(1)) > maxNumber) {
                    maxNumber = Integer.parseInt(matcher.group(1));
                }
            }
        }
        maxSalaryRangeDayNumber = maxNumber;
        minSalaryRangeDayName = "min" + maxNumber;
    }

    public void sendRequest(Double max, Double currentReference, Long id, LocalDate targetDate, Integer sla) {//
        String url = "https://harvest.greenhouse.io/v1/jobs/" + id;
        //StringBuilder builder = new StringBuilder();
        List<String> customFields = new ArrayList<>();

        if (!max.equals(0.0)) {
            customFields.add("{ " +
                    "            \"name_key\": \"salary_max\", " +
                    "            \"value\":" + max +
                    "        }");
        }
        if (!currentReference.equals(0.0)) {
            customFields.add(" {" +
                    "           \"id\": 10953615002," +
                    "           \"value\":" + +currentReference +
                    "        } ");
        }
        if (targetDate != null) {
            String dataFormatted = targetDate.format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));
            customFields.add("{ " +
                    "            \"name_key\": \"target_date_to_close\", " +
                    "            \"value\":\"" + dataFormatted +"\"" +
                    "        }");
            customFields.add("{ " +
                    "            \"name_key\": \"vacancy_sla\", " +
                    "            \"value\":" + sla +
                    "        }");
        }

        if (!customFields.isEmpty()) {
            String body = "{" +
                    "    \"custom_fields\":["
                    + String.join(", ", customFields) +
                    "]}";
            System.out.println(body);;
            requestService.sendUpdateRequest(url, body);
        }
    }

    @Scheduled(cron = "2 2 2 ? * *")
    public void cleanUpdatedMap() {
        log.info(todayUpdatedJobs.size() + " has been updated");
        todayUpdatedJobs = new HashMap<>();
        log.info("map is empty");
    }


}
