package com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.GHJob;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZPDesignation;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZPLocation;
import com.innovecs.greenhousetozohopeoplesynchronize.repository.dwh.GHJobRepository;
import com.innovecs.greenhousetozohopeoplesynchronize.repository.dwh.ZPDesignationRepository;
import com.innovecs.greenhousetozohopeoplesynchronize.repository.dwh.ZPLocationRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.math3.util.Precision;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Log4j2
@Service
@EnableScheduling
@RequiredArgsConstructor
public class GHJobService {

    public final ZPLocationRepository zpLocationRepository;
    private final GHRequestService ghRequestService;
    private final ZPDesignationRepository zpDesignationRepository;
    private final GHJobRepository ghJobRepository;


    private final SalaryCardService ssService;

    @Value("${innovecs.green-house.token}")
    private String token;

    public static Map<String, Map<String, Double>> titlesWithUpdatedFieldsMap = new HashMap<>();


    @Scheduled(cron = "${sync.salary-cards.cron}")
    public String makeMassUpdateForDesignation() {
        log.info("start of updating");
        List<ZPDesignation> zpDesignations = zpDesignationRepository.findAll()
                .stream().filter(item ->
                        item.getMax5() != null).collect(Collectors.toList());
        Map<String, List<GHJob>> ghJobs = getListJobs(false).stream().collect(Collectors.groupingBy(GHJob::getPositionTitle));
        for (ZPDesignation zpDesignation : zpDesignations) {
            if (ghJobs.containsKey(zpDesignation.getDesignation())) {
                List<GHJob> ghUpdatedJobs = ghJobs.get(zpDesignation.getDesignation());
                for (GHJob ghJob : ghUpdatedJobs) {
                    try {
                        //setup Location Coef
                        Double max = 0.0;
                        Double currentReference = 0.0;

                        if (zpDesignation.getMax5() != null) {
                            max = Precision.round((zpDesignation.getMax5() * ghJob.getLocationCoefficient()), 2);
                        }

                        if (zpDesignation.getCurrentReference() != null && !zpDesignation.getCurrentReference().equals("")) {
                            currentReference =
                             Precision.round((Double.parseDouble(zpDesignation.getCurrentReference()) * ghJob.getLocationCoefficient()), 2);
                        }

                        log.info("update for: ghjob " + ghJob.getName() + " values: current reference: " + currentReference + ", max: " + max);
                        ssService.sendRequest(max, currentReference, ghJob.getId(), null, null);

                    } catch (Exception ex) {
                        log.error(ex);
                    }
                }
            }
        }
        return "gh Job have been updated";
    }


    private List<GHJob> getListJobs(Boolean mapCondition) {
        List<GHJob> ghJobList = new ArrayList<>();

        int counter = 1;
        ResponseEntity<String> response = null;
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Authorization", token);
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            while (true) {
                String url = "https://harvest.greenhouse.io/v1/jobs?page=" + counter + "&per_page=500";

                response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);

                JsonNode parentNode = objectMapper.readTree(Objects.requireNonNull(response.getBody()));

                if (parentNode.isArray() && parentNode.size() > 0) {
                    log.info(String.format("GH jobs size: %d, counter: %d ", parentNode.size(), counter));

                    for (final JsonNode pojo : parentNode) {
                        boolean condition = (mapCondition) ?
                                pojo.get("custom_fields").get("position_title") != null
                                && titlesWithUpdatedFieldsMap.containsKey(
                                pojo.get("custom_fields").get("position_title").textValue())
                                && pojo.get("status").textValue().equals("open")
                                :
                                pojo.get("custom_fields").get("position_title") != null
                                        && pojo.get("status").textValue().equals("open");

                        if (condition) {
                            GHJob ghJob = objectMapper.convertValue(pojo, GHJob.class);

                            //location
                            String location = (pojo.get("offices").size() > 1) ?
                                    "Multiple locations" :
                                    pojo.get("offices").get(0).get("name").textValue();

                            Optional<ZPLocation> zpLocationOptional = zpLocationRepository.findByCity(location);
                            if (zpLocationOptional.isPresent()) {
                                ghJob.setLocationCoefficient(zpLocationOptional.get().getSalaryCoefficient());
                            } else ghJob.setLocationCoefficient(1.0);

                            //custom fields
                            String positionTitle = pojo.get("custom_fields").get("position_title").textValue();
                            ghJob.setPositionTitle(positionTitle);

                            ghJobList.add(ghJob);
                        }
                    }
            } else{
                log.info("reached maximum GH jobs, size: " + ghJobList.size());
                break;
            }
            counter++;
        }

    } catch(
    Exception e)

    {
        log.error(e.getMessage());
    }
        return ghJobList;
}

    @Scheduled(cron = "0 0/20 * ? * *")
    public void syncTitles() {
        if (!titlesWithUpdatedFieldsMap.isEmpty()) {
            List<GHJob> ghJobsMap = getListJobs(true);
            if (!ghJobsMap.isEmpty()) {
                for (GHJob job : ghJobsMap) {
                    String url = "https://harvest.greenhouse.io/v1/jobs/" + job.getId();
                    StringBuilder builder = new StringBuilder();
                    titlesWithUpdatedFieldsMap.get(job.getPositionTitle()).forEach((key, value) -> {
                        builder.append("{ " +
                                "            \"name_key\": \"" + key + "\", " +
                                "            \"value\":" + value * job.getLocationCoefficient() +
                                "        }");
                    });
                    if (!builder.toString().equals("")) {
                        String body = "{" +
                                "    \"custom_fields\":["
                                + builder.toString() +
                                "]}";
                        ghRequestService.sendUpdateRequest(url, body);
                        SalaryCardService.todayUpdatedJobs.put(job.getId(), Timestamp.valueOf(LocalDateTime.now()));
                    }
                }
            }
            titlesWithUpdatedFieldsMap = new HashMap<>();
            log.info(ghJobsMap.size() + " jobs were updated");
        }

    }
}

