package com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse;

import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.GHCandidate;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZPEmployee;
import com.innovecs.greenhousetozohopeoplesynchronize.repository.dwh.GHCandidateRepository;
import com.innovecs.greenhousetozohopeoplesynchronize.repository.dwh.ZPEmployeeRepository;
import com.innovecs.greenhousetozohopeoplesynchronize.repository.relations.ZPDesignationGHTitleRelationRepository;
import com.squareup.okhttp.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

@Log4j2
@Service
@EnableScheduling
@RequiredArgsConstructor
public class EmployeeTagService {

    private final GHCandidateRepository ghCandidateRepository;
    private final ZPEmployeeRepository zpEmployeeRepository;

    @Value("${innovecs.green-house.admin-id}")
    private String adminId;

    @Value("${innovecs.green-house.token}")
    private String token;

    private static final String TAG = "\"current employee\"";
    private static final String REQUEST_TEMPLATE = "";
    private static final List<String> INNOVECS_STATUS_LIST = Arrays.asList(
            "Newcomer", "Maternity/paternity", "Employee", "Ready for start");

    private static final String REQUEST_BODY = "current employee";

    @Scheduled(cron = "0 06 5 ? * WED")
    public void sync() {
        setCurrentEmployeeTag();
        removeCurrentEmployeeTag();
    }

    private void setCurrentEmployeeTag() {

        List<GHCandidate> ghCandidates = ghCandidateRepository.findAll();

        List<ZPEmployee> zpEmployees = zpEmployeeRepository.findAll();
        zpEmployees.removeIf(item -> !INNOVECS_STATUS_LIST.contains(item.getInnovecsStatus1()));
        List<String> activeEmployeeEmailList = zpEmployees.stream().map(ZPEmployee::getOtherEmail).collect(Collectors.toList());

        ghCandidates.removeIf(item ->
                item.getTags().contains(TAG));
        ghCandidates.removeIf(item -> item.getEmailAddresses().equals(""));
        //int i = 0;
        Map<Long, List<String>> ghCandidateIdAndTags = new HashMap<>();
        for (GHCandidate ghCandidate : ghCandidates) {
            if (ghCandidate.getEmailAddresses().contains(",")) {
                String[] ghEmails = ghCandidate.getEmailAddresses().split(", ");
                for (String ghEmail : ghEmails) {
                    if (activeEmployeeEmailList.contains(ghEmail)) {
                        if (ghCandidate.getTags().equals("[]")) {
                            ghCandidateIdAndTags.put(ghCandidate.getId(), Arrays.asList(TAG));
                        } else {
                            String tagsStr = ghCandidate.getTags().
                                    replace("[", "").replace("]", "");
                            List<String> tags = new ArrayList<>();
                            if (tagsStr.contains(", "))
                                tags = Arrays.asList(ghCandidate.getTags().replaceAll("\"", "").split(", "));
                            else tags.add(tagsStr);
                            tags.add(TAG);
                            ghCandidateIdAndTags.put(ghCandidate.getId(), tags);
                        }
                    }
                }
            } else {
                if (activeEmployeeEmailList.contains(ghCandidate.getEmailAddresses())) {
                    if (ghCandidate.getTags().equals("[]")) {
                        ghCandidateIdAndTags.put(ghCandidate.getId(), Arrays.asList(TAG));
                    } else {
                        String tagsStr = ghCandidate.getTags().
                                replace("[", "").replace("]", "");
                        List<String> tags = new ArrayList<>();
                        if (tagsStr.contains(", "))
                            tags = Arrays.asList(ghCandidate.getTags().replaceAll("\"", "").split(", "));
                        else tags.add(tagsStr);
                        tags.add(TAG);
                        ghCandidateIdAndTags.put(ghCandidate.getId(), tags);
                    }
                }
            }
        }
        log.info(ghCandidateIdAndTags.size());
        List<String> results = new ArrayList<>();
        ghCandidateIdAndTags.forEach((id, tags) -> {
            results.add(sendRequest(id, tags));
        });

        log.info("Number:" + results.size());
    }

    private void removeCurrentEmployeeTag() {
        List<ZPEmployee> zpTerminatedEmployees = zpEmployeeRepository.findAll();
        zpTerminatedEmployees.removeIf(zpEmployee -> !zpEmployee.getInnovecsStatus1().equals("Terminated"));

        List<GHCandidate> ghCandidates = new ArrayList<>();
        for (ZPEmployee zpTerminatedEmployee : zpTerminatedEmployees) {
            if (zpTerminatedEmployee.getOtherEmail().equals("")) {
                ghCandidates.addAll(ghCandidateRepository.findAllByEmailAddressesContainsIgnoreCase(zpTerminatedEmployee.getEmailID()));
            } else {
                ghCandidates.addAll(ghCandidateRepository.findAllByEmailAddressesContainingIgnoreCaseOrEmailAddressesContainingIgnoreCase(zpTerminatedEmployee.getOtherEmail(), zpTerminatedEmployee.getEmailID()));
            }
        }

        ghCandidates.removeIf(ghCandidate -> !ghCandidate.getTags().contains(TAG));
        log.info("Number: " + ghCandidates.size());

        for (GHCandidate ghCandidate : ghCandidates) {
            List<String> updatedTags = new ArrayList<>();
            if (ghCandidate.getTags().contains(",")) {
                for (String s : ghCandidate.getTags().replace("[", "")
                        .replace("]", "").split(",")) {
                    if (!s.equals("\"current employee\"")) {
                        updatedTags.add(s);
                    }
                }
            }
            sendRequest(ghCandidate.getId(), updatedTags);
        }

    }

    private String sendRequest(Long id, List<String> tags) {
        try {
            String requestBody = " {\"tags\":" + tags + "}";
            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/javascript");
            RequestBody body = RequestBody.create(mediaType, requestBody);
            Request request = new Request.Builder()
                    .url("https://harvest.greenhouse.io/v1/candidates/" + id)
                    .method("PATCH", body)
                    .addHeader("On-Behalf-Of", adminId)
                    .addHeader("Authorization", token)
                    .addHeader("Content-Type", "application/javascript")
                    .build();
            Response response = client.newCall(request).execute();
            log.info(response.header("status"));
            Thread.sleep(5000);
            response.body().close();
            return response.header("status");
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
        return "-";
    }


}


