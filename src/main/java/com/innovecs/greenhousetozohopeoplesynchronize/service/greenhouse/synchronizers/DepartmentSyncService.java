package com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse.synchronizers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZPDepartment;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.relations.GHDepartment;
import com.innovecs.greenhousetozohopeoplesynchronize.repository.relations.GHDepartmentRepository;
import com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse.DwhForeignKeyManager;
import com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse.EmailSender;
import com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse.GHRequestService;
import com.squareup.okhttp.OkHttpClient;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

@Data
@Log4j2
@Service
@EnableScheduling
@RequiredArgsConstructor
public class DepartmentSyncService {

    private final RestTemplate restTemplate;
    private final HttpEntity<Object> httpEntity;
    private final ObjectMapper objectMapper;
    private final OkHttpClient okHttpClient;

    private final GHRequestService ghRequestService;
    private final DwhForeignKeyManager dwhForeignKeyManager;
    private final EmailSender emailSender;

    @Qualifier("dwhCrawlerJdbcTemplate")
    private final JdbcTemplate dwhCrawlerJdbcTemplate;



    private final GHDepartmentRepository ghDepartmentRepository;

    public List<GHDepartment> getGHDepartments() {
        List<GHDepartment> ghDepartmentList = new ArrayList<>();

        ResponseEntity<String> response = null;

        try {
            String url = "https://harvest.greenhouse.io/v1/departments?page=1&per_page=500";
            response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
            JsonNode parentNode = objectMapper.readTree(Objects.requireNonNull(response.getBody()));
            if (parentNode.isArray() && parentNode.size() > 0) {
                log.info("GH departments size:" + parentNode.size());
                ghDepartmentList = objectMapper.convertValue(
                        parentNode,
                        new TypeReference<List<GHDepartment>>() {
                        });
            }
            Thread.sleep(1100);
        } catch (Exception e) {
            log.error("Error description", e);
        }
        return ghDepartmentList;
    }

    public List<ZPDepartment> getZPDepartments() {
        List<ZPDepartment> zpDepartmentList = new ArrayList<>();
        String sqlRequest = "select * from departmentlevelsview;";

        dwhCrawlerJdbcTemplate.query(sqlRequest, (resultSet) -> {
            zpDepartmentList.add(new ZPDepartment(resultSet.getLong("zohoid"),
                    resultSet.getString("department"), resultSet.getLong("parent_departmentid"),
                    resultSet.getString("parent_department"), resultSet.getInt("Level")));
        });

        return zpDepartmentList;
    }

    @Scheduled(cron = "${sync.department.cron}" )
    public void sync() {
        log.info("sync of Departments is started");
        Map<Long, ZPDepartment> zpDepartmentMap = getZPDepartments().stream()
                .collect(Collectors.toMap(ZPDepartment::getZohoID, item -> item));

        Map<Long, GHDepartment> ghDepartmentMap = ghDepartmentRepository.findAll().stream()
                .collect(Collectors.toMap(GHDepartment::getZohoID, item -> item));


        //check renamed zp departments
        List<GHDepartment> departmentsToRename = new ArrayList<>();
        ghDepartmentMap.forEach((zohoId, ghDepartment) -> {
            if (ghDepartment.getZohoID() == 365473248232749L)
                log.info("");
            if (zpDepartmentMap.containsKey(zohoId)
                    && ghDepartment.getPaired()
                    && !zpDepartmentMap.get(zohoId).getDepartment().equals(ghDepartment.getName())) {
                ghDepartment.setName(zpDepartmentMap.get(zohoId).getDepartment());
                ghDepartment.setDepartment(ghDepartment.getName());
                departmentsToRename.add(ghDepartment);
            }
        });

        log.info("Renamed departments, size: " + departmentsToRename.size());
        if (!departmentsToRename.isEmpty()) {
            sendUpdateRequest(departmentsToRename);
        }


        List<GHDepartment> departmentsToCreate = new ArrayList<>();
        zpDepartmentMap.forEach((zohoid, zpDepartment) -> {
            if (!ghDepartmentMap.containsKey(zohoid)) {
                departmentsToCreate.add(new GHDepartment(zpDepartment));
            }
        });

        log.info("Created departments, size: " + departmentsToCreate.size());
        if (!departmentsToCreate.isEmpty()) {
            sendPostRequest(departmentsToCreate);
        }

        syncFieldsOfNewDepartments();
        log.info("sync of Departments is finished");
    }

    private void sendUpdateRequest(List<GHDepartment> departmentsToRename) {
        try {
            for (GHDepartment department : departmentsToRename) {
                log.info(department.getDepartment());
                department.setName(department.getDepartment());
                ghDepartmentRepository.save(department);
                ghRequestService.sendUpdateRequest(
                        "https://harvest.greenhouse.io/v1/departments/" + department.getId(),
                        "{ \"name\": \"" + department.getName() + "\" }");
            }
        } catch (Exception exception) {
            log.error(exception.getMessage());
        }
    }

    private void sendPostRequest(List<GHDepartment> departmentsToCreate) {

        Map<Integer, List<GHDepartment>> levelListMap = new TreeMap<>(departmentsToCreate.stream().collect(
                Collectors.groupingBy(GHDepartment::getLevel)));
        levelListMap.forEach((level, list) -> {
            for (GHDepartment ghDepartment : list) {
                try {
                    StringBuilder body = new StringBuilder("{ \"name\": \"");
                    body.append(ghDepartment.getDepartment() + "\"");
                    if (level > 0) {
                        Optional<GHDepartment> optionalGhParentDepartment = ghDepartmentRepository.findById(ghDepartment.getZohoParentId());
                        if (optionalGhParentDepartment.isPresent() && (optionalGhParentDepartment.get().getPaired())) {
                            Long parentGHId = optionalGhParentDepartment.get().getId();
                            body.append(", \"parent_id\":" + parentGHId);
                            ghDepartment.setGhParentId(parentGHId);
                            ghDepartment.setPaired(true);
                        } else {
                            log.error("parent department without pair: " + ghDepartment.getDepartment());
                        }
                    }
                    body.append(" }");
                    System.out.println(body.toString());
                    ghRequestService.sendPostRequest("https://harvest.greenhouse.io/v1/departments",
                            body.toString());
                    ghDepartmentRepository.save(ghDepartment);
                } catch (Exception ex) {
                    log.error(ex.getMessage());
                }
            }
        });

    }


    public void syncFieldsOfNewDepartments() {

        Map<String, ZPDepartment> zpDepartmentMap = getZPDepartments().stream()
                .collect(Collectors.toMap(ZPDepartment::getDepartment, item -> item));
        Map<String, GHDepartment> ghDepartmentMap = getGHDepartments().stream()
                .collect(Collectors.toMap(GHDepartment::getName, item -> item));

        List<GHDepartment> ghDepartmentList = ghDepartmentRepository.findAll();


        for (GHDepartment ghDepartment : ghDepartmentList) {
            if (ghDepartment.getPaired()
                    && ghDepartmentMap.containsKey(ghDepartment.getDepartment())) {
                GHDepartment newDepartment = ghDepartmentMap.get(ghDepartment.getDepartment());
                ghDepartment.setId(newDepartment.getId());
                ghDepartment.setName(newDepartment.getName());
                ghDepartment.setGhParentId(newDepartment.getGhParentId());
                ghDepartment.setPaired(true);
            }
        }
        ghDepartmentRepository.saveAll(ghDepartmentList);

        List<GHDepartment> ghNonexistentDepartmentList = new ArrayList<>();
        ghDepartmentMap.forEach(((name, ghDepartment) -> {
            if (!zpDepartmentMap.containsKey(name)) {
                ghNonexistentDepartmentList.add(ghDepartment);
            }
        }));

        // check deleted zp departments
        List<GHDepartment> departmentsToDelete = new ArrayList<>();
        for (GHDepartment ghDepartment : ghDepartmentList) {
            if (!zpDepartmentMap.containsKey(ghDepartment.getDepartment())) {
                departmentsToDelete.add(ghDepartment);
            }
        }

        if (!departmentsToDelete.isEmpty() || !ghNonexistentDepartmentList.isEmpty()) {
            emailSender.createLetterOfDeletedDepartments(departmentsToDelete, ghNonexistentDepartmentList);
        } else {
            log.info("There is no one deleted zp department");
        }

    }


}
