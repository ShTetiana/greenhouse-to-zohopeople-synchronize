package com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse.synchronizers;


import com.innovecs.greenhousetozohopeoplesynchronize.entity.auxiliary.GHCustomFieldOption;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZPDesignation;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.relations.ZPDesignationGHTitleRelation;
import com.innovecs.greenhousetozohopeoplesynchronize.repository.dwh.ZPDesignationRepository;
import com.innovecs.greenhousetozohopeoplesynchronize.repository.relations.ZPDesignationGHTitleRelationRepository;
import com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse.GHJobService;
import com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse.GHRequestService;
import lombok.RequiredArgsConstructor;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Log4j2
@Service
@RequiredArgsConstructor
public class DesignationRequestService {

    @Value("${zoho.request.auth}")
    @NotNull
    private String zohoauth;

    private final GHRequestService ghRequestService;
    private final DesignationSyncService syncService;

    private final ZPDesignationGHTitleRelationRepository zpDesignationGHTitleRelationRepository;
    private final ZPDesignationRepository zpDesignationRepository;

    public void checkOutFields(String auth, Long zohoid, String designation,
                                 Double currentReference, String max5str, String max2){
        if(!auth.equals(zohoauth)) {
            log.error("Wrong auth token");
            return;
        }
        Optional<ZPDesignationGHTitleRelation> titleRelationOptional =
                zpDesignationGHTitleRelationRepository.findById(zohoid);


        Double max5 = Double.valueOf(max5str);
        if(titleRelationOptional.isPresent()){
            ZPDesignationGHTitleRelation relation = titleRelationOptional.get();
            if(relation.getName().equals(designation)){
                ZPDesignation zpDesignation = zpDesignationRepository.findByZohoID(zohoid).get();
                Map<String, Double> updatedValues = new HashMap<>();
                if(currentReference != null){
                    if(!(zpDesignation.getCurrentReference()!=null &&
                           Double.parseDouble(zpDesignation.getCurrentReference()) == currentReference)) {
                        updatedValues.put("current_reference", currentReference);
                    }
                    zpDesignation.setCurrentReference(String.valueOf(currentReference));
                }
                if (max5 != null){
                    if(!(zpDesignation.getMax5()!=null &&
                            zpDesignation.getMax5().equals(max5))) {
                        updatedValues.put("salary_max", max5);
                    }
                    zpDesignation.setMax5(max5);
                }
                zpDesignationRepository.save(zpDesignation);

                if(!updatedValues.isEmpty()){
                    GHJobService.titlesWithUpdatedFieldsMap.put(
                            designation, updatedValues);
                    log.info("designation added to map: " + designation + " " + updatedValues.size());
                }
            } else {
                String stringBuilder = "{\"options\":[" +
                        "{\"id\": " + relation.getGhID() +
                        ", \"name\": \"" + designation + "\"}" +
                        " ]}";
                relation.setName(designation);
                zpDesignationGHTitleRelationRepository.save(relation);
                ghRequestService.sendUpdateRequest(
                        "https://harvest.greenhouse.io/v1/custom_field/8895385002/custom_field_options",
                        stringBuilder);
                log.info("Des: " + designation + " has been renamed");
            }
        } else {
            log.info("new title creation");
            ZPDesignationGHTitleRelation newRelation = new ZPDesignationGHTitleRelation();
            newRelation.setZohoID(zohoid);
            newRelation.setName(designation);

            Integer priority = zpDesignationGHTitleRelationRepository.findAll()
                    .stream().map(ZPDesignationGHTitleRelation::getPriority).max(Integer::compareTo).get() + 1;
            newRelation.setPriority(priority);

            String stringBuilder = "{ \"options\":[" +
                    "{\"name\": \"" + newRelation.getName() +
                    "\", \"priority\": " + newRelation.getPriority() + "}" +
                    " ]}";
            ghRequestService.sendPostRequest(
                "https://harvest.greenhouse.io/v1/custom_field/8895385002/custom_field_options",
                    stringBuilder);
            Long ghId = syncService.getListOfCurrentGHTitles().stream().collect(
                    Collectors.toMap(GHCustomFieldOption::getName, GHCustomFieldOption::getId))
                    .get(designation);
            newRelation.setName(designation);
            zpDesignationGHTitleRelationRepository.save(newRelation);
            log.info("Designation: " + newRelation.getName() + " has been created");
        }
    }




}
