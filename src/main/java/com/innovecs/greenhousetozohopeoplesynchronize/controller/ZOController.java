package com.innovecs.greenhousetozohopeoplesynchronize.controller;

import com.innovecs.greenhousetozohopeoplesynchronize.service.zohoone.PhoneSyncService;
import com.innovecs.greenhousetozohopeoplesynchronize.service.zohoone.TerminationSyncService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@EnableScheduling
@RestController
@RequiredArgsConstructor
public class ZOController {

    private final TerminationSyncService terminationSyncService;
    private final PhoneSyncService phoneSyncService;


    @Scheduled(cron = "0 10 9 ? * MON-FRI")
    @RequestMapping(value = "/run", method = RequestMethod.GET)
    public void custom() {
        log.info("Adapter started");
        terminationSyncService.synchronizeActiveEmployees();
        phoneSyncService.synchronizeTerminatedEmployees();
        log.info("Adapter finished");
    }


    @RequestMapping(value = "/setTerminated", method = RequestMethod.GET)
    public String setTerminated() {
        log.info("Adapter started");
        phoneSyncService.setTerminated();
        return "Adapter finished";
    }


}