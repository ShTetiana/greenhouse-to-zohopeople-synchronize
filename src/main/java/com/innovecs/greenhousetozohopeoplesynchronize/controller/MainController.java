package com.innovecs.greenhousetozohopeoplesynchronize.controller;

import com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse.synchronizers.InterviewerSyncService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
@RequiredArgsConstructor
public class MainController {

        private final InterviewerSyncService interviewerSyncService;

        @GetMapping("/sync/gh/interviewers")
        public void syncInterviewers(){
            interviewerSyncService.sync();
        }

}
