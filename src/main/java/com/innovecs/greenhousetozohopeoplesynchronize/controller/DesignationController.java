package com.innovecs.greenhousetozohopeoplesynchronize.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse.GHJobService;
import com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse.SalaryCardService;
import com.innovecs.greenhousetozohopeoplesynchronize.service.greenhouse.synchronizers.DesignationRequestService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;

@Log4j2
@RestController
@RequiredArgsConstructor
public class DesignationController {

    private final SalaryCardService salaryCardService;
    private final DesignationRequestService designationRequestService;
    private final GHJobService ghJobService;

    @PostMapping("/greenhouse/job_created")
    public void catchWebHook(@RequestBody JsonNode body) {
        log.info("message has been received");
        salaryCardService.saveWebHook(body);
    }

    @PostMapping("/designation")
    public void catchZohoWebHook(@RequestParam("max4") String max4,
                                 @RequestParam("max5") String max5,
                                 @RequestParam("current_reference") Double currentReference,
                                 @RequestParam("designation_name") String designation,
                                 @RequestParam("zohoid") Long zohoid,
                                 @RequestParam("auth") String auth) {
        log.info("designation: " + designation);
        designationRequestService.checkOutFields(auth, zohoid, designation,
                currentReference, max5, max4);
    }

    @GetMapping("/designation/salary-cards")
    public void makeMassUpdateOfGHJobs(){
        ghJobService.makeMassUpdateForDesignation();
    }
}
