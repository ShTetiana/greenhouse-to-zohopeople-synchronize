package com.innovecs.greenhousetozohopeoplesynchronize.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.squareup.okhttp.OkHttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

@Configuration
public class BeanConfig {

    @Value("${innovecs.green-house.token}")
    private String token;

    @Bean
    ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        return objectMapper;
    }

    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    HttpEntity<Object> httpEntity() {
        return new HttpEntity<>(new HttpHeaders() {{
            set("Accept", MediaType.APPLICATION_JSON_VALUE);
            set("Authorization", token);
        }});
    }

    @Bean
    OkHttpClient okHttpClient() {
        return new OkHttpClient();
    }
}
