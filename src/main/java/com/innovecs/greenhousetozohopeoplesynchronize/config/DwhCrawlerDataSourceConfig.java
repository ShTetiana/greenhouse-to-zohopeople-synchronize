package com.innovecs.greenhousetozohopeoplesynchronize.config;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;


@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "dwhCrawlerEntityManager",
        transactionManagerRef = "dwhCrawlerTransactionManager",
        basePackages = "com.innovecs.greenhousetozohopeoplesynchronize.repository"
)
@RequiredArgsConstructor
public class DwhCrawlerDataSourceConfig {


    @Bean
    @Primary
    @ConfigurationProperties("spring.datasource.dwh-crawler")
    public DataSourceProperties dwhCrawlerDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    public Map<String, Object> jpaProperties() {
        Map<String, Object> props = new HashMap<>();
        props.put("hibernate.physical_naming_strategy", SpringPhysicalNamingStrategy.class.getName());
        props.put("hibernate.implicit_naming_strategy", SpringImplicitNamingStrategy.class.getName());
        props.put("hibernate.hbm2ddl.auto", "none");
        return props;
    }


    @Bean(name = "dwhCrawlerDataSource")
    // @ConfigurationProperties(prefix = "spring.datasource.dwh-crawler")
    public DataSource dwhCrawlerDataSource(DataSourceProperties dwhCrawlerDataSourceProperties) {
        return dwhCrawlerDataSourceProperties.initializeDataSourceBuilder().
                build();
    }

    @Bean(name = "dwhCrawlerEntityManager")
    public LocalContainerEntityManagerFactoryBean dwhCrawlerEntityManagerFactory(EntityManagerFactoryBuilder entityManagerFactoryBuilder) {
        return entityManagerFactoryBuilder
                .dataSource(dwhCrawlerDataSource(dwhCrawlerDataSourceProperties()))
                .packages("com.innovecs.greenhousetozohopeoplesynchronize.entity") // you can also give the package where the Entities are given rather than giving Entity class
                .persistenceUnit("dwh").properties(jpaProperties())
                .build();
    }

    @Bean(name = "dwhCrawlerTransactionManager")
    public PlatformTransactionManager dwhCrawlerTransactionManager(@Qualifier("dwhCrawlerEntityManager") EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }

    @Bean(name = "dwhCrawlerJdbcTemplate")
    public JdbcTemplate dwhCrawlerJdbcTemplate(@Qualifier("dwhCrawlerDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

}