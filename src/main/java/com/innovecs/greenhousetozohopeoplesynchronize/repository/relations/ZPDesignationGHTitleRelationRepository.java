package com.innovecs.greenhousetozohopeoplesynchronize.repository.relations;

import com.innovecs.greenhousetozohopeoplesynchronize.entity.relations.ZPDesignationGHTitleRelation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ZPDesignationGHTitleRelationRepository extends JpaRepository<ZPDesignationGHTitleRelation, Long>{;
}
