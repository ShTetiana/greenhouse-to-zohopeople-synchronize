package com.innovecs.greenhousetozohopeoplesynchronize.repository.relations;

import com.innovecs.greenhousetozohopeoplesynchronize.entity.relations.GHInterviewer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface GHInterviewerRepository extends JpaRepository<GHInterviewer, Long> {

    List<GHInterviewer> getAllByZohoIDIsNotNull();

    Optional<GHInterviewer> getByZohoID(Long zohoid);
}
