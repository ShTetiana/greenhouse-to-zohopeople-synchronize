package com.innovecs.greenhousetozohopeoplesynchronize.repository.relations;

import com.innovecs.greenhousetozohopeoplesynchronize.entity.relations.GHDepartment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface GHDepartmentRepository extends JpaRepository<GHDepartment, Long> {

    Optional<GHDepartment> findByDepartment(String department);

}
