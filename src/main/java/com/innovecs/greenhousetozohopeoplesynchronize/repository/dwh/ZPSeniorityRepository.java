package com.innovecs.greenhousetozohopeoplesynchronize.repository.dwh;

import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZPSeniority;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ZPSeniorityRepository extends JpaRepository<ZPSeniority, Long> {

    Optional<ZPSeniority> findBySeniorityIgnoreCase(String seniority);



}

