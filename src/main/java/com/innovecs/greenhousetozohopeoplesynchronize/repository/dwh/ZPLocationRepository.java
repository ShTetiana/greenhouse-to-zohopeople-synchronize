package com.innovecs.greenhousetozohopeoplesynchronize.repository.dwh;

import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZPDesignation;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZPLocation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ZPLocationRepository extends JpaRepository<ZPLocation, Long> {

    Optional<ZPLocation> findByCity(String city);

}

