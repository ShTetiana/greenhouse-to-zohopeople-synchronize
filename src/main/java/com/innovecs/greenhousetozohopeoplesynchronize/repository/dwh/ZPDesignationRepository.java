package com.innovecs.greenhousetozohopeoplesynchronize.repository.dwh;

import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZPDesignation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ZPDesignationRepository extends JpaRepository<ZPDesignation, Long> {

    Optional<ZPDesignation> findByDesignation(String designation);

    List<ZPDesignation> getAllByZohoIDIn(List<Long> ids);

    Optional<ZPDesignation> findByZohoID(Long zohoId);
}

