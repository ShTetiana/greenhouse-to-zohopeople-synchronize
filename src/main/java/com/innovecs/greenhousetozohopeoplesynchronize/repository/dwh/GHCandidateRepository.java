package com.innovecs.greenhousetozohopeoplesynchronize.repository.dwh;

import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.GHCandidate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GHCandidateRepository extends JpaRepository<GHCandidate, Long> {


    List<GHCandidate> findAllByEmailAddressesContainingIgnoreCaseOrEmailAddressesContainingIgnoreCase(String emailAddress, String innovecsEmail);

    List<GHCandidate> findAllByEmailAddressesContainsIgnoreCase(String innovecsEmail);
}
