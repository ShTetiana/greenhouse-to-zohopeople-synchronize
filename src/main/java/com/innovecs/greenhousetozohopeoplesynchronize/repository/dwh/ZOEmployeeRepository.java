package com.innovecs.greenhousetozohopeoplesynchronize.repository.dwh;

import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZOEmployee;
import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZPEmployee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Set;

public interface ZOEmployeeRepository extends JpaRepository<ZOEmployee, Long> {

    List<ZOEmployee> getAllByStatus(String status);

    List<ZOEmployee> getAllByMobilePhoneIsNotNull();

    List<ZOEmployee> getAllByEmailIn(Set<String> names);



}
