package com.innovecs.greenhousetozohopeoplesynchronize.repository.dwh;

import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.GHJob;
import org.springframework.data.jpa.repository.JpaRepository;


public interface GHJobRepository extends JpaRepository<GHJob, Long> {

}
