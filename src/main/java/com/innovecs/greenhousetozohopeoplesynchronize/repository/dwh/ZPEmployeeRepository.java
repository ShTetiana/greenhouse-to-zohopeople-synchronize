package com.innovecs.greenhousetozohopeoplesynchronize.repository.dwh;

import com.innovecs.greenhousetozohopeoplesynchronize.entity.dwh.ZPEmployee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ZPEmployeeRepository extends JpaRepository<ZPEmployee, Long> {

    List<ZPEmployee> findAllByEmailIDIsNot(String email);

    List<ZPEmployee> findAllByDepartmentIDAndInnovecsStatus1In(Long departmentId, List<String> statuses);

    List<ZPEmployee> findAllByGhInterviewerAndInnovecsStatus1InAndDepartmentIsNot(
            Boolean ghInterviewer, List<String> statuses, String department);


    List<ZPEmployee> getAllByMobileIsNotNull();

    ZPEmployee getByEmailID(String emailID);

    List<ZPEmployee> getAllByNormalizeNumberIn(List<String> numbers);


}

