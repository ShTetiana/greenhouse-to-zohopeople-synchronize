ALTER TABLE public.zp_department DROP CONSTRAINT IF EXISTS zp_department_zp_employee_zohoid_fk;
ALTER TABLE public.zp_department DROP CONSTRAINT IF EXISTS zp_department_zp_employee_zohoid_fk_2;
ALTER TABLE public.zp_department DROP CONSTRAINT IF EXISTS zp_department_zp_employee_zohoid_fk_3;
ALTER TABLE public.zp_employee_title DROP CONSTRAINT IF EXISTS zp_employee_title_zp_department_zohoid_fk;
ALTER TABLE public.zp_employee DROP CONSTRAINT IF EXISTS zp_employee_zp_department_zohoid_fk;
ALTER TABLE public.zp_offer DROP CONSTRAINT IF EXISTS zp_offer_zp_department_zohoid_fk;