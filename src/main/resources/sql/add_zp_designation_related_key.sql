--
-- Name: zp_employee_title zp_employee_title_zp_designation_zohoid_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.zp_employee_title
    ADD CONSTRAINT zp_employee_title_zp_designation_zohoid_fk FOREIGN KEY (designationdid) REFERENCES public.zp_designation (zohoid);


    --
-- Name: zp_designation zp_designation_zp_sow_title_zohoid_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.zp_designation
    ADD CONSTRAINT zp_designation_zp_sow_title_zohoid_fk FOREIGN KEY (sowtitleid) REFERENCES public.zp_sow_title (zohoid);

--
-- Name: zp_employee zp_employee_zp_designation_zohoid_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.zp_employee
    ADD CONSTRAINT zp_employee_zp_designation_zohoid_fk FOREIGN KEY (designationid) REFERENCES public.zp_designation (zohoid);


--
-- Name: zp_offer zp_offer_zp_designation_zohoid_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.zp_offer
    ADD CONSTRAINT zp_offer_zp_designation_zohoid_fk FOREIGN KEY (designationid) REFERENCES public.zp_designation (zohoid);

