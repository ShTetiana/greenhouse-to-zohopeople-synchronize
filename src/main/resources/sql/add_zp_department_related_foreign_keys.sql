--
-- Name: zp_department zp_department_zp_employee_zohoid_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.zp_department
    ADD CONSTRAINT zp_department_zp_employee_zohoid_fk FOREIGN KEY (added_byid) REFERENCES public.zp_employee (zohoid);


--
-- Name: zp_department zp_department_zp_employee_zohoid_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.zp_department
    ADD CONSTRAINT zp_department_zp_employee_zohoid_fk_2 FOREIGN KEY (department_leadid) REFERENCES public.zp_employee (zohoid);


--
-- Name: zp_department zp_department_zp_employee_zohoid_fk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.zp_department
    ADD CONSTRAINT zp_department_zp_employee_zohoid_fk_3 FOREIGN KEY (modified_byid) REFERENCES public.zp_employee (zohoid);


--
-- Name: zp_employee_title zp_employee_title_zp_department_zohoid_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.zp_employee_title
    ADD CONSTRAINT zp_employee_title_zp_department_zohoid_fk FOREIGN KEY (departmentid) REFERENCES public.zp_department (zohoid);


--
-- Name: zp_employee zp_employee_zp_department_zohoid_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.zp_employee
    ADD CONSTRAINT zp_employee_zp_department_zohoid_fk FOREIGN KEY (departmentid) REFERENCES public.zp_department (zohoid);

--
-- Name: zp_offer zp_offer_zp_department_zohoid_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.zp_offer
    ADD CONSTRAINT zp_offer_zp_department_zohoid_fk FOREIGN KEY (departmentid) REFERENCES public.zp_department (zohoid);


--